$( document ).ready( function() {
    $('div.rating').each(function (i, d) {
        if (d.getAttribute("score") == "5"){
            $(this).append(
                "<label class = \"full\" for=\"star5\" title=\"Genial\" style=\"color: #FFD700;\"></label>" +
                "<label class = \"full\" for=\"star4\" title=\"Bueno\" style=\"color: #FFD700;\"></label>" +
                "<label class = \"full\" for=\"star3\" title=\"Regular\" style=\"color: #FFD700;\"></label>" +
                "<label class = \"full\" for=\"star2\" title=\"Malo\" style=\"color: #FFD700;\"></label>" +
                "<label class = \"full\" for=\"star1\" title=\"Pésimo\" style=\"color: #FFD700;\"></label>"
            );
        }
        if (d.getAttribute("score") == "4"){
            $(this).append(
                "<label class = \"full\" for=\"star5\" title=\"Genial\"></label>" +
                "<label class = \"full\" for=\"star4\" title=\"Bueno\" style=\"color: #FFD700;\"></label>" +
                "<label class = \"full\" for=\"star3\" title=\"Regular\" style=\"color: #FFD700;\"></label>" +
                "<label class = \"full\" for=\"star2\" title=\"Malo\" style=\"color: #FFD700;\"></label>" +
                "<label class = \"full\" for=\"star1\" title=\"Pésimo\" style=\"color: #FFD700;\"></label>"
            );
        }
        if (d.getAttribute("score") == "3"){
            $(this).append(
                "<label class = \"full\" for=\"star5\" title=\"Genial\"></label>" +
                "<label class = \"full\" for=\"star4\" title=\"Bueno\"></label>" +
                "<label class = \"full\" for=\"star3\" title=\"Regular\" style=\"color: #FFD700;\"></label>" +
                "<label class = \"full\" for=\"star2\" title=\"Malo\" style=\"color: #FFD700;\"></label>" +
                "<label class = \"full\" for=\"star1\" title=\"Pésimo\" style=\"color: #FFD700;\"></label>"
            );
        }
        if (d.getAttribute("score") == "2"){
            $(this).append(
                "<label class = \"full\" for=\"star5\" title=\"Genial\"></label>" +
                "<label class = \"full\" for=\"star4\" title=\"Bueno\"></label>" +
                "<label class = \"full\" for=\"star3\" title=\"Regular\"></label>" +
                "<label class = \"full\" for=\"star2\" title=\"Malo\" style=\"color: #FFD700;\"></label>" +
                "<label class = \"full\" for=\"star1\" title=\"Pésimo\" style=\"color: #FFD700;\"></label>"
            );
        }
        if (d.getAttribute("score") == "1"){
            $(this).append(
                "<label class = \"full\" for=\"star5\" title=\"Genial\"></label>" +
                "<label class = \"full\" for=\"star4\" title=\"Bueno\"></label>" +
                "<label class = \"full\" for=\"star3\" title=\"Regular\"></label>" +
                "<label class = \"full\" for=\"star2\" title=\"Malo\"></label>" +
                "<label class = \"full\" for=\"star1\" title=\"Pésimo\" style=\"color: #FFD700;\"></label>"
            );
        }
        if (d.getAttribute("score") == "0"){
            $(this).append(
                "<label class = \"full\" for=\"star5\" title=\"Genial\"></label>" +
                "<label class = \"full\" for=\"star4\" title=\"Bueno\"></label>" +
                "<label class = \"full\" for=\"star3\" title=\"Regular\"></label>" +
                "<label class = \"full\" for=\"star2\" title=\"Malo\"></label>" +
                "<label class = \"full\" for=\"star1\" title=\"Pésimo\"></label>"
            );
        }
    });
});
