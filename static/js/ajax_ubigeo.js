window.onload = function() {
    $('#id_departamento').change(function() {
        var url = $('#ajax_ubigeo').attr('data-provincias-url');
        var departamentoId = $(this).val();
        $.ajax({
            url: url,
            data: {
                'departamento': departamentoId
            },
            success: function(response) {
                $('#id_provincia').html(response);
            }
        });
    });

    $('#id_provincia').change(function() {
        var url = $('#ajax_ubigeo').attr('data-distritos-url');
        var provinciaId = $(this).val();
        $.ajax({
            url: url,
            data: {
                'provincia': provinciaId
            },
            success: function(response) {
                $('#id_distrito').html(response);
            }
        });
    });
};

