import datetime

from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View
from django.views.generic import CreateView, UpdateView, FormView

from customauth.models import EmailUser
from customauth.utils import get_profesionales_disponibles
from servicios.utils import HorarioPorFecha
from servicios.constants import (
    DIAS,
    HORA_INICIO_GT_FIN,
    HORA_INICIO_E_FIN,
    HORA_NO_DISPONIBLE,
    RESERVA_EXISTENTE
)
from servicios.models import Profesional, Horario
from reservas.forms import FechaForm
from reservas.models import Reserva, Pregunta, Respuesta
from reservas.utils import PreciosPorHorario, PreciosPorReserva, is_int, get_mes
from customauth.models import DatosPersonales


class BuscadorEmailUserView(View):
    """
    Vista que renderiza un buscador de usuarios ya sea por el correo electronico
    o por el DNI del usuario.

    :cvar template_name: El nombre del template a renderizar.
    :cvar profesional: Objeto que guardara el modeldo Profesional.
    :cvar fecha: Objeto que guardara la fecha de una reserva.
    :cvar hora: Objeto que guardara a la hora de inicio de un horario.
    """
    template_name = "emailuser_search.html"
    profesional = None
    fecha = None
    hora = None

    def get(self, request, year, month, day, horario_hora, horario_minuto,
            profesional_pk):
        """
        Metodo Get usado para obtener y setear el id del Profesional y la fecha.

        :param request: Parametro que guarda la informacion de la pagina.
        :param year: str que guarda el Año.
        :param month: str que guarda el Mes.
        :param day: str que guarda el Dia.
        :param horario_hora: int que guarda la hora de inicio del horario
            reservado.
        :param horario_minuto: int que guarda la hora de inicio del horario
            reservado.
        :param profesional_pk: int que guarda el ID del Profesional.

        :return: Actualizacion del metodo get.
        """
        self.profesional = Profesional.objects.get(id=profesional_pk)
        self.fecha = datetime.date(year=year, month=month, day=day)
        self.hora = datetime.time(hour=horario_hora, minute=horario_minuto)
        context = self.get_context_data()
        return render(request, 'emailuser_search.html', context)

    def post(self, request, year, month, day, horario_hora, horario_minuto,
             profesional_pk):
        """
        Metodo Post usadopara implementar la logica del buscador.

        :param request: Parametro que guarda la informacion de la pagina.
        :param year: str que guarda el Año.
        :param month: str que guarda el Mes.
        :param day: str que guarda el Dia.
        :param horario_hora: int que guarda la hora de inicio del horario
            reservado.
        :param horario_minuto: int que guarda la hora de inicio del horario
            reservado.
        :param profesional_pk: int que guarda el ID del Profesional.

        :return: Actualizacion de la pagina y su contexto.
        """
        self.profesional = Profesional.objects.get(id=profesional_pk)
        self.fecha = datetime.date(year=year, month=month, day=day)
        self.hora = datetime.time(hour=horario_hora, minute=horario_minuto)
        contexto = self.get_context_data()
        #Todo: si el performance se pone muy lento en esta parte, quitar busqueda por telefono
        #if que consulta si el radiobutton es para una busqueda por DNI.
        user_filter_kwargs = dict(
            email_user__is_active=True,
            email_user__type_user='f'
        )
        entrada_busqueda = request.POST.get('query')
        query = DatosPersonales.objects.filter(**user_filter_kwargs)
        query = query.filter(
            Q(dni__icontains=entrada_busqueda) |
            Q(telefono__icontains=entrada_busqueda) |
            Q(email_user__email__icontains=entrada_busqueda)
        )
        contexto['users'] = query
        return render(self.request, 'emailuser_search.html', contexto)

    def get_context_data(self, **kwargs):
        """
        Metodo usado para modificar el contexto de la pagina y actualizarlo.

        :return: actualizacion del contexto.
        """
        hora_fin = datetime.time(hour=self.hora.hour)
        context = dict(
            profesional=self.profesional,
            fecha=self.fecha,
            hour=self.hora,
            hora_fin=(datetime.datetime.combine(
                    datetime.date(1, 1, 1), hora_fin)
                + datetime.timedelta(hours=1)).time()
        )
        return context


class ReservaCreateView(CreateView):
    """
    Vista heredada de CreateView encargada de renderizar la pagina para la
    creacion de una reserva.

    :cvar model: Variable que guarda el modelo que se quiere crear.
    :cvar fields: Lista con los campos del modelo que se quiere setear en front.
    :cvar template_name: Nombre del template a renderizar.
    :cvar profesional: Objeto que guardara el Profesional seleccionada para la reserva.
    :cvar fecha: Variable que guardara la fecha de la reserva.
    :cvar hora_inicio: Variable que guardara la hora de inicio de la reserva.
    :cvar email_user: objeto que guarda el EmailUser seleccionado.
    """
    model = Reserva
    fields = ['descuento', 'fin_hora_reserva', 'inicio_hora_reserva']
    template_name = 'reserva_create.html'
    profesional = None
    fecha = None
    hora_inicio = None
    hora_fin = None
    email_user = None
    precio_total = None
    dia = None
    lista_profesionales_disponibles = list()

    def get(self, request, year, month, day, hour_inicio, minute_inicio,
            hour_fin, minute_fin, profesional_pk, email_user_pk, *args, **kwargs):
        """
        Metodo get usado para obtener algunos campos del modelo reserva y
        setearlos a las variables de la clase.

        :param request: Parametro que guarda la informacion de la pagina.
        :param year: Parametro que guarda el año de la reserva.
        :param month: Parametro que guarda el mes de la reserva.
        :param day: Parametro que guarda el dia de la reserva.
        :param hour_inicio: Int que guarda hora de inicio de la reserva.
        :param minute_inicio: Int que guarda los minutos de inicio de la reserva.
        :param hour_fin: Int que guarda hora de fin de la reserva.
        :param minute_fin: Int que guarda los minutos de fin de la reserva.
        :param profesional_pk: Int que guarda el id del Profesional.
        :param email_user_pk: Parametro que guarda el id del EmailUser.

        :return: Actualizaccion del metodo get
        """
        self.profesional = Profesional.objects.get(id=profesional_pk)
        self.fecha = datetime.date(year=year, month=month, day=day)
        self.hora_inicio = datetime.time(hour=hour_inicio, minute=minute_inicio)
        self.hora_fin = datetime.time(hour=hour_fin, minute=minute_fin)
        self.email_user = EmailUser.objects.get(id=email_user_pk)
        self.precio_total = PreciosPorReserva(
            self.hora_inicio, self.hora_fin, self.profesional,
            self.fecha).get_precio()
        self.reserva_consulta = Reserva.objects.filter(
            fecha=self.fecha, inicio_hora_reserva__gte=self.hora_inicio,
            fin_hora_reserva__lte=self.hora_fin, profesional=self.profesional
        ).first()
        reserva_mas_cercana = Reserva.objects.filter(
            inicio_hora_reserva__gt=self.hora_inicio, profesional=self.profesional,
            fecha=self.fecha).order_by(
            'inicio_hora_reserva'
        ).first()
        # if que consulta si existe una hora final disponible posterior a la hora de inicio
        if reserva_mas_cercana != None:
            self.reserva_mas_cercana = reserva_mas_cercana.inicio_hora_reserva
        # si no, la hora final sera la hora de cierre de la empresa
        else:
            self.reserva_mas_cercana = self.profesional.empresa.hora_cierre
        self.lista_profesionales_disponibles.clear()
        self.lista_profesionales_disponibles.extend(
            get_profesionales_disponibles(
                request.user.empresa, self.fecha, self.hora_inicio,
                self.hora_fin, self.profesional.id
            )
        )
        return super(ReservaCreateView, self).get(request, *args, **kwargs)

    def post(self, request, year, month, day, hour_inicio, minute_inicio,
            hour_fin, minute_fin, profesional_pk, email_user_pk, *args, **kwargs):
        """
        Metodo Post usado para setear algunos campos del modelo reserva.

        :param request: Parametro que guarda la informacion de la pagina.
        :param profesional_pk: int que guarda el ID del Profesional.
        :param year: Int que guarda el Año.
        :param month: Int que guarda el Mes.
        :param day: Int que guarda el Dia.
        :param hour_inicio: Int que guarda hora de inicio de la reserva.
        :param minute_inicio: Int que guarda los minutos de inicio de la reserva.
        :param hour_fin: Int que guarda hora de fin de la reserva.
        :param minute_fin: Int que guarda los minutos de fin de la reserva.
        :param email_user_pk: Parametro que guarda el id del EmailUser.

        :return: Actualizacion del metodo Post.

        """
        self.profesional = Profesional.objects.get(id=profesional_pk)
        self.fecha = datetime.date(year=year, month=month, day=day)
        self.hora_inicio = datetime.time(hour=hour_inicio, minute=minute_inicio)
        self.hora_fin = datetime.time(hour=hour_fin, minute=minute_fin)
        self.email_user = EmailUser.objects.get(id=email_user_pk)
        self.precio_total = PreciosPorReserva(
            self.hora_inicio, self.hora_fin, self.profesional,
            self.fecha).get_precio()
        desde_input = self.request.POST.get('inicio_hora_reserva').split(':')
        hasta_input = self.request.POST.get('fin_hora_reserva').split(':')
        kwargs = {'year': year, 'month': month, 'day': day,
                  'hour_inicio': desde_input[0], 'minute_inicio': desde_input[1],
                  'hour_fin': hasta_input[0], 'minute_fin': hasta_input[1],
                  'profesional_pk': profesional_pk, 'email_user_pk': email_user_pk}
        reserva_mas_cercana = Reserva.objects.filter(
            inicio_hora_reserva__gt=self.hora_inicio, profesional=self.profesional,
            fecha=self.fecha).order_by(
            'inicio_hora_reserva'
        ).first()
        # if que consulta si existe una hora final disponible posterior a la hora de inicio
        if reserva_mas_cercana != None:
            self.reserva_mas_cercana = reserva_mas_cercana.inicio_hora_reserva
        # si no, la hora final sera la hora de cierre de la empresa
        else:
            self.reserva_mas_cercana = self.profesional.empresa.hora_cierre
        #botones uno para validar y el otro para crear la reserva
        if 'calcular-precio' in self.request.POST:
            return HttpResponseRedirect(
                reverse('reservas-reserva-create', kwargs=kwargs))
        if 'crear_button' in self.request.POST:
            hasta_time = datetime.time(
                hour=int(hasta_input[0]), minute=int(hasta_input[1]))
            desde_time = datetime.time(
                hour=int(desde_input[0]), minute=int(desde_input[1]))
            if (hasta_time == desde_time) or (hasta_time < desde_time):
                if reserva_mas_cercana and (
                            hasta_time > reserva_mas_cercana.inicio_hora_reserva):
                    return HttpResponseRedirect(reverse(
                        'reservas-reserva-create', kwargs=kwargs))
                return HttpResponseRedirect(reverse(
                    'reservas-reserva-create', kwargs=kwargs))
            return super(ReservaCreateView, self).post(request, *args, **kwargs)

    def get_form(self):
        """
        Metodo que modifica el el campo fin_hora_reserva para setearle un valor
        por defecto
        """
        form = super(ReservaCreateView, self).get_form()
        placeholder_hora_fin = self.hora_fin
        placeholder_hora_inicio = self.hora_inicio
        form.fields['fin_hora_reserva'].widget.attrs['value'] = placeholder_hora_fin
        form.fields['inicio_hora_reserva'].widget.attrs['value'] = placeholder_hora_inicio
        return form

    def get_context_data(self, **kwargs):
        """
        Metodo usado para modificar el contexto de la pagina y actualizarlo.

        :return: actualizacion del contexto.
        """
        context = super(ReservaCreateView, self).get_context_data()
        context['profesional'] = self.profesional
        context['year'] = self.kwargs.get('year')
        context['month'] = self.kwargs.get('month')
        context['day'] = self.kwargs.get('day')
        context['hora_inicio'] = self.hora_inicio
        context['hora_fin']= self.hora_fin
        context['precio_total'] = round(self.precio_total, 2)
        if self.hora_fin.hour == self.hora_inicio.hour:
            context['precio_x_hora'] = round(self.precio_total, 2)
        else:
            context['precio_x_hora'] = (round(self.precio_total
                / (self.hora_fin.hour - self.hora_inicio.hour), 2))
        context['dia_semana'] = DIAS[self.fecha.weekday()][1]
        context['usuario'] = DatosPersonales.objects.filter(
            email_user=self.email_user).first()
        context['profesionales_disponibles'] = self.lista_profesionales_disponibles
        context['hora_fin_disponible'] = self.reserva_mas_cercana
        if self.hora_inicio > self.hora_fin:
            context['errors'] = HORA_INICIO_GT_FIN
        if self.hora_inicio == self.hora_fin:
            context['errors'] = HORA_INICIO_E_FIN
        if self.hora_fin > self.reserva_mas_cercana:
            context['errors'] = HORA_NO_DISPONIBLE
        if self.reserva_consulta:
            context['errors'] = RESERVA_EXISTENTE
        return context

    def form_valid(self, form):
        """
        Metodo encargado de validar el formulario para la creacion del objeto
        Reserva y de setear sus campos pasados por post.

        :param form: Formulario de la pagina.

        :return: Creacion del objeto exitosamente.
        """
        form.instance.profesional = self.profesional
        form.instance.fecha = self.fecha
        form.instance.precio_total = self.precio_total
        form.instance.email_user = self.email_user
        form.instance.estado = Reserva.RESERVADO
        form.instance.precio_total = round(self.precio_total, 2)
        if self.hora_fin.hour == self.hora_inicio.hour:
            form.instance.precio_hora = round(self.precio_total, 2)
        else:
            form.instance.precio_hora = (round(self.precio_total
                / (self.hora_fin.hour - self.hora_inicio.hour), 2))
        reserva = form.save()
        url_name = 'reservas-resumen-reserva'
        kwargs = {'id_reserva': reserva.id}
        return HttpResponseRedirect(reverse(url_name, kwargs=kwargs))


class ResumenReservaView(View):
    """
    Vista que renderiza la pagina de los detalles de una reserva.
    """

    def get(self, request, id_reserva):
        """
        Metodo get para el renderizado de la pagina de detalle de reserva y
        encuesta , también cargará el contexto de la pagina.

        :param id_reserva: id de la reserva de la cual se quiere los detalles.
        :return: Metodo render usado para renderizar.
        """
        reserva = Reserva.objects.get(pk=id_reserva)
        preguntas = Pregunta.objects.filter(
            protocolo__tipo_servicio=reserva.profesional.empresa.tipo_servicio,
            protocolo__es_para_profesional=False
        )
        if reserva:
            contexto = {'reserva': reserva, 'preguntas': preguntas}
            return render(request, 'resumen_reserva.html', contexto)
        return redirect('reservas-home')

    def post(self, request, id_reserva):
        """
        Metodo Post que creara la encuesta y redireccionara a la misma pagina.

        :param request:Parametro que guarda los datos de la pagina.
        :param id_reserva: primary key de la reserva actual

        :return: Metodo de redireccionamiento.
        """
        respuestas = Respuesta.objects.filter(reserva_id=id_reserva).delete()
        for respuesta in request.POST:
            if respuesta == 'csrfmiddlewaretoken':
                continue
            Respuesta.objects.create(
                reserva_id=id_reserva,
                respuesta_elegida_id=request.POST[respuesta]
            ).save()
        url_name = 'reservas-resumen-reserva'
        kwargs = {'id_reserva': id_reserva}
        return HttpResponseRedirect(reverse(url_name, kwargs=kwargs))


class ReservasHomeView(View):
    """
    Vista que renderiza la pagina para el modulo de reservaciones
    """

    def get(self, request, *args, **kwargs):
        """
        Metodo Get para el renderizado de la pagina principal de reservaciones
        """
        return render(request, 'reservas_home.html', {})


class SemanaSelectView(View):
    """
    Vista que utiliza la fecha actual y redirecciona a la pagina de los 7 dias
    de la semana a la que pertenece esa fecha
    """

    def get(self, request, *args, **kwargs):
        """
        Metodo Get que saca la fecha accedida del request y te redirecciona a la
        semana de esa fecha para ver los 7 dias de esa semana

        :return: redirect a el inicio de la semana de la fecha accedida
        """

        today = datetime.date.today()
        return redirect('reservas-reservas-dias', year=today.year,
                        month=today.month, day=today.day)


class SemanaFechaSelectView(FormView):
    """
    Vista que muestra los 7 dias de la semana dependiendo de la fecha accedida
    """
    template_name = 'reserva_semana_list.html'
    form_class = FechaForm

    def get(self, request, year, month, day, *args, **kwargs):
        """
        Metodo Get que a partir de un dia lunes pasado en el parametro,
        calculara toda la semana y calculara tambien el inicio de la semana
        siguiente y la semana anterior

        :param year: Int que contiene el año
        :param month: Int que contiene el mes
        :param day: Int que contiene el dia

        :return: render con los datos pertinentes para la semana
        """
        self.get_day = datetime.date(year=year, month=month, day=day)
        self.for_days_list = [date for date in (
            self.get_day + datetime.timedelta(n) for n in range(4))]
        return super(SemanaFechaSelectView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """
        Funcion que agrega la tabla de profesionales disponibles al contexto
        #todo: agregar dias de semana (lun, mar) y 2 botones para navegar 4 dias atras y 4 delante
        :return: Dict template context
        """
        context = super(SemanaFechaSelectView, self).get_context_data(**kwargs)
        context['today'] = datetime.date.today()
        context['get_day'] = self.get_day
        context['previous_week'] = self.get_day - datetime.timedelta(days=4)
        context['next_week'] = self.get_day + datetime.timedelta(days=4)
        context['profesionales_por_dia'] = {dia: HorarioPorFecha(
                empresa=self.request.user.empresa, fecha=dia).get_horas(
            ) for dia in self.for_days_list
        }
        return context

    def get_success_url(self, year, month, day):
        """
        Funcion que se dirige a la fecha seleccionada

        :param year: Int que contiene el año
        :param month: Int que contiene el mes
        :param day: Int que contiene el dia
        :return: reverse a la fecha con argumentos
        """
        kwargs = {'year': year, 'month': month, 'day': day}
        return reverse('reserva-profesional-fecha-list', kwargs=kwargs)


class FechaListView(View):
    """
    Vista que muestra los Profesionales disponibles por horarios dependiendo de la
    fecha
    """

    def get(self, request, year, month, day, *args, **kwargs):
        """
        Metodo Get que obtiene el numero de Profesionales y su disponibilidad por horas

        :param year: Int que contiene el año
        :param month: Int que contiene el mes
        :param day: Int que contiene el dia

        :return: render que contiene los horarios libres y referencias al dia
            siguiente y anterior
        """
        today = datetime.date(year=year, month=month, day=day)
        context = {
            'horas': HorarioPorFecha(
                empresa=request.user.empresa, fecha=today).get_horas(),
            'yesterday': today - datetime.timedelta(days=1),
            'tomorrow': today + datetime.timedelta(days=1),
            'page_context': today,
            'today': datetime.datetime.today().date()
        }
        return render(request, 'reserva_fecha_list.html', context=context)


class FechaHorarioListView(View):
    """
    Vista que muestra los profesionales disponibles de una hora y dia en especial
    """

    def get(self, request, year, month, day, hour, *args, **kwargs):
        """
        Metodo Get que muestra los profesionales disponibles en el horario selecionado
        al sacar las reservas y sustraerle a la lista de profesionales.
        reservadas

        :param year: Int que contiene el año
        :param month: Int que contiene el mes
        :param day: Int que contiene el dia
        :param hour: Int que contiene la hora previamente seleccionada

        :return: render con todas los profesionales libres
        """
        page_context = dict(year=year, month=month, day=day, hour=hour)
        profesionales = Profesional.objects.filter(empresa=request.user.empresa)
        date = datetime.date(year=year, month=month, day=day)
        reservas = Reserva.objects.filter(
            fecha=date,
            profesional__in=profesionales,
            inicio_hora_reserva__lte=datetime.time(hour=hour),
            fin_hora_reserva__gte=(
                datetime.datetime.combine(
                    datetime.date(1, 1, 1), datetime.time(hour=hour))
                + datetime.timedelta(hours=1)
            ).time(),
        )
        profesionales = profesionales.exclude(id__in=[reserva.profesional.id for reserva in reservas])
        list_profesionales = list()
        for profesional in profesionales:
            reservas_previas = Reserva.objects.filter(
                fecha=date, profesional=profesional,
                fin_hora_reserva__gt=datetime.time(hour=hour),
            ).exclude(estado='3x').first()
            intervalos_minutos = [00, 15, 30, 45]
            if reservas_previas:
                minutos = reservas_previas.fin_hora_reserva.minute
                if minutos % 15 == 0:
                    intervalos_minutos = intervalos_minutos[int(minutos / 15):]
                else:
                    intervalos_minutos = intervalos_minutos[
                                         (int(minutos / 15) + 1):]
            list_profesionales.append(dict(
                nombres=profesional.nombres,
                precio=PreciosPorHorario(profesional, hour, date).get_precio(),
                id=profesional.id,
                intervalos_minutos=intervalos_minutos
            ))
        context = {
            'profesionales': list_profesionales,
            'page_context': page_context,
            'date': date,
        }
        return render(request, 'reserva_fecha_hora_list.html', context)


class BuscadorReservasView(View):
    """
    Vista que renderiza la pagina para el modulo de reservaciones.
    """

    def get(self, request):
        """
        Metodo Get que se usa para el renderizado de la pagina de busqueda de
        una Reserva.

        :return: Metodo de renderizado.
        """
        return render(request, 'reserva_search.html', {})

    def post(self, request, *args, **kwargs):
        """
        Metodo post usado para implementar la logica del buscador.

        :param request: Parametro que gurada la informacion de la pagina.

        :return: Metodo de renderizado con el contexts de la pagina actualizado.
        """
        contexto = dict()
        entrada_busqueda = request.POST.get('query')
        query = Reserva.objects.filter(profesional__empresa=request.user.empresa)
        if is_int(entrada_busqueda):
            query = Reserva.objects.filter(id=int(entrada_busqueda))
        else:
            query = query.filter(email_user__email__icontains=entrada_busqueda)
        contexto['reservas'] = query
        return render(self.request, 'reserva_search.html', contexto)


class ConfirmacionReservaUpdateView(UpdateView):
    """
    UpdateVista enacargada de actualiza el Modelo de reserva.

    :cvar model: Modelo que se actualizara.
    :cvar fields: Campos del modelo que se actualizaran.
    :cvar template_name: Nombre del template que se renderizara.
    :cvar context_object_name: Nombre del modelo que se usara en el contexto.
    """
    model = Reserva
    fields = []
    template_name = "confirmacion_reserva.html"
    context_object_name = 'reserva'

    def form_valid(self, form):
        """
        Metodo encargado de validar el formulario y actualizar la reserva a un
        estado de confirmado.

        :param form: Parametroq ue guarda el formulario usado.

        :return: Actualizacion del formulario.
        """
        form.instance.estado = '2c'
        return super(ConfirmacionReservaUpdateView, self).form_valid(form)

    def get_success_url(self):
        """
        Metodo usado para redireccionar una vez validado el formulario.

        :return: Metodo de redireccion hacia el buscador de reservas.
        """
        return reverse('reservas-buscador-reservas')


class CancelacionReservaUpdateView(UpdateView):
    """
    UpdateVista enacargada de cancelar una reserva el modelo de reserva.

    :cvar model: Modelo que se actualizara.
    :cvar fields: Campos del modelo que se actualizaran.
    :cvar template_name: Nombre del template que se renderizara.
    :cvar context_object_name: Nombre del modelo que se usara en el contexto.
    """
    model = Reserva
    fields = []
    template_name = "cancelacion_reserva.html"
    context_object_name = 'reserva'

    def form_valid(self, form):
        """
        Metodo encargado de validar el formulario y actualizar la reserva a un
        estado de cancelado.

        :param form: Parametroq ue guarda el formulario usado.

        :return: Actualizacion del formulario.
        """
        form.instance.estado = '3x'
        return super(CancelacionReservaUpdateView, self).form_valid(form)

    def get_success_url(self):
        """
        Metodo usado para redireccionar una vez validado el formulario.

        :return: Metodo de redireccion hacia el buscador de reservas.
        """
        return reverse('reservas-buscador-reservas')

