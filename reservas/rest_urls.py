from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter

from reservas.rest_views import ReservaViewSet, ProfesionalResenaViewSet

router = DefaultRouter()

router.register(r'reservas', ReservaViewSet)
router.register(
    r'reviews_cancha', ProfesionalResenaViewSet, basename='reviews_cancha'
)

urlpatterns = [
    url(r'reservas/', include(router.urls))
]
