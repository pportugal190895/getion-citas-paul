from django import forms

from bootstrap_datepicker_plus import DateTimePickerInput


class FechaForm(forms.Form):
    """
    Form para la redireccion de fechas
    """
    fecha = forms.DateField(widget=DateTimePickerInput(format='%Y-%m-%d'))
