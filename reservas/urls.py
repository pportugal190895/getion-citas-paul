from django.urls import path

from reservas.views import (
    SemanaSelectView,
    SemanaFechaSelectView,
    ReservasHomeView,
    FechaListView,
    FechaHorarioListView,
    ReservaCreateView,
    BuscadorEmailUserView,
    BuscadorReservasView,
    ConfirmacionReservaUpdateView,
    CancelacionReservaUpdateView,
    ResumenReservaView,
)


urlpatterns = [
    path('reservas/', ReservasHomeView.as_view(), name='reservas-home'),
    path('reservas/dia/', SemanaSelectView.as_view(),
         name='reservas-reserva-dia'),
    path('reservas/dia/<int:year>/<int:month>/<int:day>',
         SemanaFechaSelectView.as_view(), name='reservas-reservas-dias'),
    path('reservas/dia/fecha/<int:year>/<int:month>/<int:day>',
         FechaListView.as_view(),
         name='reserva-profesional-fecha-list'),
    #todo crear un buscador de fechas
    path('reservas/dia/fecha/<int:year>/<int:month>/<int:day>/horario/'
         '<int:hour>', FechaHorarioListView.as_view(),
         name='reserva-profesional-fecha-hora-list'),
    path('reservas/dia/fecha/<int:year>/<int:month>/<int:day>/horario_inicio/'
         '<int:hour_inicio>/<int:minute_inicio>/horario_fin/<int:hour_fin>/'
         '<int:minute_fin>/<int:profesional_pk>/<int:email_user_pk>',
         ReservaCreateView.as_view(),
         name='reservas-reserva-create'),
    path('reservas/dia/fecha/<int:year>/<int:month>/<int:day>/horario/'
         '<int:horario_hora>/<int:horario_minuto>/<int:profesional_pk>/buscador/',
         BuscadorEmailUserView.as_view(),
         name='reservas-emailuser-search'),
    path('reservas/buscador/reservas',
         BuscadorReservasView.as_view(),
         name='reservas-buscador-reservas'),
    path('reservas/confirmar/reserva/<int:pk>/',
         ConfirmacionReservaUpdateView.as_view(),
         name='reservas-confirmar-reserva'),
    path('reservas/cancelar/reserva/<int:pk>/',
         CancelacionReservaUpdateView.as_view(),
         name='reservas-cancelar-reserva'),
    path('reservas/resumen/reserva/<int:id_reserva>/',
         ResumenReservaView.as_view(), name='reservas-resumen-reserva'),
]
