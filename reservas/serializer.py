from servicios.models import Profesional
from reservas.models import Reserva, Resena
from rest_framework import serializers


class ReservaSerializer(serializers.ModelSerializer):
    """
    Serializer para las reservas y todos sus campos
    """
    class Meta:
        model = Reserva
        fields = '__all__'


class ReservaVigenteSerializer(serializers.ModelSerializer):
    """
    Serializer para la reserva y la vista resumen la cual contiene solo id,
    nombre del Profesional, horas y fecha
    """
    class Meta:
        model = Reserva
        fields = (
            'id', 'profesional_empresa_nombre', 'inicio_hora_reserva',
            'fin_hora_reserva', 'fecha')

    profesional_empresa_nombre = serializers.SerializerMethodField(
        'get_empresa_nombre')

    def get_empresa_nombre(self, obj):
        return obj.profesional.empresa.nombre


class ReservaResumenSerializer(serializers.ModelSerializer):
    """
    Serializador para el resumen de las reservas

    Ejemplo:
    {
        "identificador": "Profesional 1",
        "precio_total": 200,
        "inicio_hora_reserva": "00:07:00",
        "fin_hora_reserva": "00:09:00",
        "nombre_empresa": "Canchas el Locaso",
        "direccion_empresa": "Urb Los glandiolos 123"
    }
    :cvar nombres: SerializerMethodField que contiene el identificador o
        nombre del Profesional
    :cvar nombre_empresa: SerializerMethodField que contiene el nombre de la
        empresa
    :cvar direccion_empresa: SerializerMethodField que contiene la direccion
        de la empresa
    """

    nombres = serializers.SerializerMethodField()
    nombre_empresa = serializers.SerializerMethodField()
    direccion_empresa = serializers.SerializerMethodField()

    def get_nombres(self, obj):
        return obj.profesional.nombres

    def get_nombre_empresa(self, obj):
        return obj.profesional.empresa.nombre

    def get_direccion_empresa(self, obj):
        return obj.profesional.empresa.direccion

    class Meta:
        model = Reserva
        fields = (
            'nombres',
            'precio_total',
            'inicio_hora_reserva',
            'fin_hora_reserva',
            'nombre_empresa',
            'direccion_empresa'
        )


class DetalleReservaSerializer(serializers.ModelSerializer):
    """
    Serializer para obtener el detalle de una reserva por el id de la reserva.
    La cual contiene la fecha, hora de inicio, hora de fin, profesional, usuario,
    precio, precio por hora, descuento y el estado de la reserva.
    {
        "id": 1,
        "fecha": "2018-11-28",
        "inicio_hora_reserva": "09:00:00",
        "fin_hora_reserva": "11:00:00",
        "estado": "1r",
        "cancha_empresa_nombre": "Michaelsoft canchas sinteticas",
        "precio_total": 100,
        "cancha_empresa_telefono": "054123456"
    }
    """

    class Meta:
        model = Reserva
        fields = (
            'id', 'fecha', 'inicio_hora_reserva', 'fin_hora_reserva',
            'estado', 'profesional_empresa_nombre', 'precio_total',
            'profesional_empresa_telefono'
        )

    profesional_empresa_nombre = serializers.SerializerMethodField(
        'get_empresa_nombre')

    def get_empresa_nombre(self, obj):
        return obj.profesional.empresa.nombre

    profesional_empresa_telefono = serializers.SerializerMethodField(
        'get_empresa_telefono')

    def get_empresa_telefono(self, obj):
        return obj.profesional.empresa.telefono


class ResenaSerializer(serializers.ModelSerializer):
    """
    Serializador para obtener la reseña, con sus campos puntaje y descripcion.
    {
        "puntaje": 5
        "descripcion": "estubo buena el servicio del Profesional"
    }
    """
    class Meta:
        model = Resena
        fields = ('puntaje', 'descripcion')


class ReservaReviewSerializer(serializers.ModelSerializer):
    """
    Serializador para obtener una reserva con sus reviews.
    {
        "reviews":[
            {
                "puntaje": 5,
                "descripcion": "estubo bueno el servicio del Profesional"
            },
            {
                "puntaje": 2,
                "descripcion": "estubo regular el servicio del Profesional"
            }
        ]
    }
    """
    reviews = ResenaSerializer(many=False, read_only=True)
    class Meta:
        model = Reserva
        fields = ('reviews', )


class DetalleProfesionalSerializer(serializers.ModelSerializer):
    """
    Serializer para el detalle de un Profesional con los campos nombres,
    tipo_cancha, precio direccion_empresa y los reviews por reserva.
    {
        "identificador": 1,
        "tipo_cancha": "fu",
        "precio": 10.4,
        "largo": 50.5,
        "ancho": 25.5,
        "direccion_empresa": "calle empresa nro 1",
        "reviews": [
            {
                "puntaje": 5,
                "descripcion": "estubo bueno el servicio del Profesional"
            },
            {
                "puntaje": 2,
                "descripcion": "el servicio del Profesional estubo regular"
            }
        ]
    }
    """
    direccion_empresa = serializers.SerializerMethodField('get_direccion')
    reviews = serializers.SerializerMethodField('get_reviews_from_reserva')

    def get_reviews_from_reserva(self, obj):
        """
        Metodo usado para obtener las reseñas por reservas de un Profesional.
        """
        reservas = Reserva.objects.filter(profesonal=obj)
        reservas = reservas.exclude(reviews__isnull=True)
        reviews = Resena.objects.filter(reserva__in=reservas)
        serializer = ResenaSerializer(reviews, many=True)
        return serializer.data

    def get_direccion(self, obj):
        """
        Metodo usado para obtener el nombre de la empresa de profesional y no
        su ID.
        """
        return obj.empresa.nombre

    class Meta:
        model = Profesional
        fields = (
            'nombres', 'apellidos', 'precio', 'direccion_empresa', 'reviews'
        )
