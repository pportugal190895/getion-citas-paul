from django import template
from reservas.models import Respuesta

register = template.Library()


@register.simple_tag
def get_respuesta(id_alternativa, id_reserva):
    """
    Metodo que devuelve la respuesta segun la alternativa y la reserva.
    
    :return: QueryList de Respuestas filtradas por alternativa y reserva.
    """
    respuesta = Respuesta.objects.filter(
        respuesta_elegida_id=id_alternativa, reserva_id=id_reserva).first()

    return respuesta
