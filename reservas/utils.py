import datetime

from servicios.constants import DIAS, MESES
from servicios.models import Horario


class PreciosPorHorario(object):
    """
    clase que recibe los parametros de profesional, la hora de inicio y la fecha.
    """
    def __init__(self, profesional, hora_inicio, fecha):
        self.profesional = profesional
        self.hora_inicio = hora_inicio
        self.fecha = fecha

    def get_precio(self):
        """
        Metodo que obtiene el precio de un horario segun la hora pasada por
        parametro.

        :return: int precio por horario seleccionado.
        """
        dia = DIAS[self.fecha.weekday()][0]
        hora_inicio = datetime.time(hour=self.hora_inicio)
        hora_fin = (datetime.datetime.combine(
                    datetime.date(1, 1, 1), hora_inicio)
                + datetime.timedelta(hours=1)).time()
        query = Horario.objects.filter(
            hora_inicio__lte=hora_inicio,
            hora_fin__gte=hora_fin,
            profesional=self.profesional,
            dia=dia
        ).first()
        #if que comprueba si existe el query obtiene el precio del horario.
        if not query:
            precio = self.profesional.precio
        #si no, obtiene el precio base del Profesional.
        else:
            precio = query.precio
        return precio


class PreciosPorReserva(object):
    """
    Clase usada para obtener el precio de una reserva.
    """
    def __init__(self, hora_inicio, hora_fin, profesional, fecha):
        self.hora_inicio = hora_inicio
        self.hora_fin = hora_fin
        self.profesional = profesional
        self.fecha = fecha

    def get_precio(self):
        """
        Metodo usado para obtener el precio total segun el horario recibido por
        el metodo get.

        :return: Int que contiene el valor del precio total.
        """
        precio_total = 0
        if self.hora_inicio.hour == self.hora_fin.hour:
            precio_total += self.get_precio_reserva_horas_iguales()
        else:
            precio_total += self.get_precio_reserva_por_horas()
            if self.hora_fin.minute > 0:
                precio_total += self.get_precio_reserva_minutos_extras()
        return precio_total

    def get_precio_reserva_horas_iguales(self):
        """
        Metodo con el primer caso de para obtener el precio de la reserva, si
        las horas del inicio y fin coinciden.

        :return: Int con el subtotal.
        """
        horario = Horario.objects.filter(
            profesional=self.profesional,
            hora_inicio__hour__lte=self.hora_inicio.hour,
            hora_fin__hour__gt=self.hora_inicio.hour,
            dia=DIAS[self.fecha.weekday()][0]).first()
        if horario:
            minuto_precio = horario.precio / 60
            return (self.hora_fin.minute -self.hora_inicio.minute) * minuto_precio
        else:
            minuto_precio = self.profesional.precio / 60
            return (self.hora_fin.minute -self.hora_inicio.minute) * minuto_precio

    def get_precio_reserva_por_horas(self):
        """
        Metodo con el segundo caso para obtener el precio de la reserva, si las
        horas de inicio y fin no coinciden.

        :return: Int con el subtotal.
        """
        subtotal = 0
        # for que recorre entre las horas de inicio y fin.
        for hora in range(self.hora_inicio.hour, self.hora_fin.hour):
            horario = Horario.objects.filter(
                profesional=self.profesional,
                hora_inicio__hour__lte=hora,
                hora_fin__hour__gt=hora,
                dia=DIAS[self.fecha.weekday()][0]).first()
            # if que obtiene el subtotal de la primera hora por los minutos
            # asignados de esta.
            if hora == self.hora_inicio.hour:
                # Si existe ese horario obtener su precio por minutos del horario.
                if horario:
                    subtotal += ((60 - self.hora_inicio.minute) / 15
                        * (horario.precio / 4))
                    continue
                # Si no: obtener el precio por minutos del Profesional.
                else:
                    subtotal += ((60 - self.hora_inicio.minute) / 15
                        * (self.profesional.precio / 4))
                    continue
            # Si existe ese horario obtener su precio por hora del horario.
            if horario:
                subtotal += horario.precio
                continue
            # Si no: obtener el precio por hora del Profesional.
            else:
                subtotal += self.profesional.precio
                continue
        return subtotal

    def get_precio_reserva_minutos_extras(self):
        """
        Metodo con el tercer caso para obtener el precio de la reserva, si la
        ultima hora tiene minutos extras.

        :return: Int con el subtotal.
        """
        horario = Horario.objects.filter(
            profesional=self.profesional,
            hora_inicio__hour__lte=self.hora_fin.hour,
            hora_fin__hour__gt=self.hora_fin.hour,
            dia=DIAS[self.fecha.weekday()][0]).first()
        # Si existe ese horario obtener su precio por minutos del horario.
        if horario:
            return self.hora_fin.minute / 15 * (horario.precio / 4)
        # Si no: obtener el precio por minutos del Profesional.
        else:
            return self.hora_fin.minute / 15 * (self.profesional.precio / 4)


def is_int(value):
    """
    Metodo que consulta si "value" es un entero.

    :param value: Parametro de consulta.
    :return: Booleano con la validación.
    """
    try:
        int(value)
        return True
    except ValueError:
        return False


def get_mes(fecha1, fecha2 = None, diminutivo=False):
    """
    Metodo que obtiene el nombre del mes, dado una o dos fechas.

    :param fecha1: Datetime que contiene una fecha.
    :param fecha2: Datetime que contiene una segunda fecha opciional.
    :param diminutivo: Bool que afirma si se quiere el mes en diminutivo.
    :return: Lista con el nombre o nombres de los meses.
    """
    #if que consulta si se quiere el mes en diminutivo
    if diminutivo:
        return [MESES[fecha1.month][0], MESES[fecha2.month][0]] if fecha2 else [
            MESES[fecha1.month][0]]
    return [MESES[fecha1.month][1], MESES[fecha2.month][1]] if fecha2 else [
        MESES[fecha1.month][1]]
