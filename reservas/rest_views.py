import datetime

from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from customauth.utils import DisponibilidadProfesionales
from servicios.models import Profesional
from customauth.permissions import EndUserIsAuthenticated
from reservas.models import Reserva
from reservas.serializer import (
    ReservaSerializer,
    ReservaVigenteSerializer,
    ReservaResumenSerializer,
    DetalleProfesionalSerializer,
    DetalleReservaSerializer,
)


class ReservaViewSet(viewsets.ModelViewSet):
    """
    ViewSet para las reservas de los usuarios
    """
    serializer_class = ReservaSerializer
    queryset = Reserva.objects.all()
    permission_classes = (EndUserIsAuthenticated,)

    @action(detail=False, url_name='reservas-vigentes-list')
    def vigentes(self, request):
        """
        Funcion retorna las Reservas vigentes (las cuales aun estan por venir)
        url: http://localhost:8000/api/rest/r/reservas/reservas/vigentes/
        Todo: retornar reservas de empresa no de cliente
        :param request: Objeto que contiene la metadata del request
        :return: Response con los datos del serializador
        """
        queryset = Reserva.objects.filter(
            email_user=request.user,
            fecha__gte=datetime.date.today(),
            estado=Reserva.RESERVADO).order_by(
            'fecha')[:5]
        serializer = ReservaVigenteSerializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['get'])
    def resumen(self, request, pk=None):
        """
        Funcion retorna un resumen de la reserva
        url: http://localhost:8000/api/rest/r/reservas/reservas/ID_RESUMEN/resumen/

        :param request: Objeto que contiene la metadata del request
        :param pk: Int que contiene el id del objeto
        :return: Response con los datos de la reserva
        """
        reserva = self.get_object()
        serializer = ReservaResumenSerializer(reserva, many=False)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        """
        Funcion retorna el objecto por id
        http://localhost:8000/api/rest/r/reservas/reservas/ID_RESERVA/

        :param request: Metadata del request
        :return: Response
        """
        instance = self.get_object()
        if instance.email_user == request.user:
            serializer = DetalleReservaSerializer(instance)
            return Response(serializer.data)
        return Response(
            data="No esta autorizado para ingresar a esta reserva",
            status=status.HTTP_403_FORBIDDEN
        )

    @action(detail=False, url_name='profesional-validar', methods=['get'],
            url_path='validar/profesional/(?P<id_cancha>\d+)/(?P<hora_inicio>\d{2})/(?P<hora_fin>\d{2})/(?P<fecha>\d{4}-\d{2}-\d{2})')
    def validar_profesional_disponible(self, request, id_profesional, hora_inicio,
                                       hora_fin, fecha):
        """
        Metodo que implementa el servicio para validar si un Profesional esta
        disponible en un horario y fecha dados.
        Hacer GET a la siguiente ruta:
        http://localhost:8000/api/rest/r/reservas/reservas/validar/cancha/1/13/30/2019-03-12/

        :param id_profesional: Str que contiene un id para la busqueda de un Profesional.
        :param hora_inicio: Str que contiene una hora de inicio.
        :param hora_fin: Str que contiene una hora fin.
        :param fecha: Str que contiene la fecha.
        :return: Response con la disponibilidad y respectivo estatus
        """
        hora_inicio = datetime.time(int(hora_inicio))
        hora_fin = datetime.time(int(hora_fin))
        profesional = Profesional.objects.get(id=id_profesional)
        fecha = [int(i) for i in fecha.split("-")]
        fecha = datetime.date(fecha[0], fecha[1], fecha[2])
        disponibilidad = DisponibilidadProfesionales(
            fecha, profesional, hora_inicio, hora_fin
        ).exists()
        #if que valida si los horarios estan bien definidos.
        if hora_inicio > hora_fin or hora_inicio == hora_fin:
            return Response(data="El horario no esta bien definido",
                            status=status.HTTP_400_BAD_REQUEST)
        # si el Profesional esta disponible en ese horario, retornar Ok status.
        if not disponibilidad:
            return Response(
                data="el Profesional esta disponible", status=status.HTTP_200_OK)
        # si no, retornar bad request request.
        return Response(
            data="el Profesional no esta disponible",
            status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'], url_name="confirmar-reserva")
    def confirmar_reserva(self, request):
        """
        Metodo que implementa el servicio para confirmar una reserva y crearla.
        Hacer POST a la siguiente ruta:
        http://localhost:8000/api/rest/r/reservas/reservas/confirmar_reserva/

        :return: Response con el status de creacion de la reserva
        """
        request.data['email_user'] = request.user.id
        hora_inicio = request.data['inicio_hora_reserva']
        hora_fin = request.data['fin_hora_reserva']
        profesional = Profesional.objects.get(id=request.data['profesional'])
        fecha = request.data['fecha']
        disponibilidad = DisponibilidadProfesionales(
            fecha, profesional, hora_inicio, hora_fin
        ).exists()
        # si el Profesional esta disponible en ese horario, crear la reserva.
        if not disponibilidad:
            serializer = ReservaSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(
                data="Reserva creada con exito",
                status=status.HTTP_201_CREATED)
        # si no, retornar bad request request.
        return Response(
            data="el Profesional no esta disponible",
            status=status.HTTP_400_BAD_REQUEST)


class ProfesionalResenaViewSet(viewsets.ModelViewSet):
    """
    ViewSet para el detalle de un Profesional con sus respectivas Reseñas
    """
    serializer_class = DetalleProfesionalSerializer
    queryset = Profesional.objects.all()
    permission_classes = (IsAuthenticated, )

