from customauth.models import EmailUser
from django.core.validators import MaxValueValidator
from django.db import models
from servicios.models import Profesional


class Reserva(models.Model):
    """
    Modelo que almacenara los detalles de una reserva

    :cvar fecha: DateField que almacena la fecha de reserva
    :cvar inicio_hora_reserva: TimeField que almacena la hora inicio de reserva
    :cvar fin_hora_reserva: TimeField que almacena la hora fin de reserva
    :cvar profesional: ForeignKey que almacena a que profesional pertenece la
        reserva
    :cvar email_user: ForeignKey que almacena el usuario que hizo la reserva
    :cvar precio_hora: FloatField que almacena el precio por hora
    :cvar precio_total: FloatField que almacena el total por todas las horas
    """
    RESERVADO = "1r"
    CONFIRMADO = "2c"
    CANCELADO = "3x"
    ESTADO_RESERVA = (
        (RESERVADO, "Reservado"),
        (CONFIRMADO, "Confirmado"),
        (CANCELADO, "Cancelado"),
    )
    fecha = models.DateField()
    inicio_hora_reserva = models.TimeField()
    fin_hora_reserva = models.TimeField()
    profesional = models.ForeignKey(
        Profesional, on_delete=models.CASCADE, null=True, related_name='reservas')
    email_user = models.ForeignKey(
        EmailUser, on_delete=models.CASCADE, null=True)
    precio_hora = models.FloatField()
    precio_total = models.FloatField()
    descuento = models.FloatField(default=0)
    #Todo: agregar validadores para que el precio > descuento y te muestre %
    estado = models.CharField(
        max_length=2, default=RESERVADO, choices=ESTADO_RESERVA)

    class Meta:
        verbose_name_plural = 'Reservas'
        verbose_name = 'Reserva'

    def __str__(self):
        return "{}, {}, {}".format(
            self.fecha, self.email_user.email, self.profesional.nombres)


class Resena(models.Model):
    """
    Modelo que almacena los datos de una reseña.

    :cvar reserva: ForeingKey OnoToOnoField que apunta a una Reserva.
    :cvar puntaje: PositiveIntegerField que guarda un puntaje.
    :cvar descripcion: TextField que guarda la descripcion de la reseña.
    """
    reserva = models.OneToOneField(
        Reserva, on_delete=models.CASCADE, related_name="reviews")
    puntaje = models.PositiveIntegerField(validators=[MaxValueValidator(5)])
    descripcion = models.TextField(blank=True, null=True)

    def __str__(self):
        return "reserva: {} --> puntaje: {}".format(self.reserva.id, self.puntaje)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        profesional = self.reserva.profesional
        num_resenas = Resena.objects.filter(
            reserva__profesional=profesional).count()
        promedio = ((num_resenas * profesional.puntaje) + self.puntaje) / (
                num_resenas + 1)
        profesional.puntaje = promedio
        profesional.save()

        return super(Resena, self).save(
            force_insert=False, force_update=False, using=None,
            update_fields=None)


class TextModel(models.Model):
    """
    TextModel que contiene textos para la descripcion de protocolos, preguntas y
    respuestas

    :cvar texto: CharField que guarda la descripcion del objeto
    """

    texto = models.CharField(max_length=256)

    class Meta:
        abstract = True


class Protocolo(TextModel):
    """
    Modelo que guarda el protocolo del tipo de servicio

    :cvar tipo_servicio: ForeignKey para el tipo de servicio
    :cvar es_para_profesional: BooleanField queidentifica si es un protocolo
        para el profesional.
    """

    tipo_servicio = models.ForeignKey(
        'servicios.TipoServicio', on_delete=models.CASCADE)
    es_para_profesional = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'protocolo'
        verbose_name_plural = 'protocolos'

    def __str__(self):
        return f"{self.tipo_servicio}: {self.texto}"


class Pregunta(TextModel):
    """
    Modelo que guarda las preguntas del protocolo

    :cvar protocolo: ForeignKey que guarda a que protocolo al que pertenece
    :cvar question_type: CharField que guarda el tipo de pregunta que sera
    """

    QUESTION_TYPES = (
        ('R', 'Ranking'),
        ('B', 'Escala'),
        ('D', 'Dicotomicas(si,no)'),
    )
    protocolo = models.ForeignKey(Protocolo, on_delete=models.CASCADE)
    question_type = models.CharField(
        max_length=1, choices=QUESTION_TYPES, blank=True)

    class Meta:
        verbose_name = 'pregunta'
        verbose_name_plural = 'preguntas'

    def __str__(self):
        return f"{self.protocolo}: {self.texto}"

    def get_alternativas(self):
        """
        Metodo que obtiene las alternativas de esta pregunta.

        :return: PosibleRespuesta queryList.
        """
        alternativas = PosibleRespuesta.objects.filter(pregunta=self)
        return alternativas


class PosibleRespuesta(TextModel):
    """
    Modelo que guarda las posibles respuestas de preguntas

    :cvar pregunta: ForeignKey hacia el modelo pregunta
    :cvar weight: IntegerField que almacena el peso por si tienen que calcular
        el valor de las respuestas para sacar el puntaje del servicio
    """

    pregunta = models.ForeignKey(Pregunta, on_delete=models.CASCADE)
    weight = models.IntegerField('peso', blank=True, null=True)

    class Meta:
        verbose_name = 'alternativa de respuesta'
        verbose_name_plural = 'alternativas de respuestas'

    def __str__(self):
        return f"{self.pregunta}: {self.texto}"


class Respuesta(models.Model):
    """
    Modelo que almacena la respuesta de los clientes

    :cvar reserva: ForeignKey hacia reserva
    :cvar respuesta_elegida: ForeignKey hacia la respuesta elegida
    """

    reserva = models.ForeignKey(Reserva, on_delete=models.CASCADE)
    respuesta_elegida = models.ForeignKey(
        PosibleRespuesta, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'respuesta'
        verbose_name_plural = 'respuestas'

    def __str__(self):
        return f"{self.respuesta_elegida}"
