from django.contrib import admin
from django.contrib.admin import ModelAdmin

from nested_inline.admin import NestedStackedInline, NestedTabularInline

from reservas.models import (
    Reserva,
    Resena,
    Protocolo,
    Pregunta,
    Respuesta,
    PosibleRespuesta,
)


class ReservaAdmin(ModelAdmin):
    """
    ModelAdmin para la reserva
    """

    list_display = (
        'profesional', 'email_user', 'fecha', 'inicio_hora_reserva',
        'fin_hora_reserva', 'precio_hora'
    )


class PosibleRespuestaNestedStackedInline(NestedTabularInline):
    """
    NestedStackedInline para posibles respuestas
    """

    model = PosibleRespuesta
    extra = 1
    fk_name = 'pregunta'


class PreguntaNestedStackedInline(NestedStackedInline):
    """
    NestedStackedInline para la pregunta
    """

    model = Pregunta
    extra = 1
    fk_name = 'protocolo'
    inlines = (PosibleRespuestaNestedStackedInline,)


class ProtocoloNestedStackedInline(NestedStackedInline):
    """
    NestedStackedInline for protocolo
    """

    model = Protocolo
    extra = 1
    max_num = 1
    fk_name = 'tipo_servicio'
    inlines = (PreguntaNestedStackedInline,)


class ProtocoloAdmin(ModelAdmin):
    """
    ModelAdmin para el protocolo de seguridad
    """

    list_display = ('tipo_servicio', 'texto')


class PreguntaAdmin(ModelAdmin):
    """
    ModelAdmin para la pregunta
    """

    list_display = ('protocolo', 'texto')


class PosibleRespuestaAdmin(ModelAdmin):
    """
    ModelAdmin para la respuesta probable
    """

    list_display = ('pregunta', )


class RespuestaAdmin(ModelAdmin):
    """
    ModelAdmin para la respuesta en si
    """

    list_display = ('reserva', 'respuesta_elegida')


admin.site.register(Reserva, ReservaAdmin)
admin.site.register(Resena)
admin.site.register(Protocolo, ProtocoloAdmin)
admin.site.register(Pregunta, PreguntaAdmin)
admin.site.register(Respuesta, RespuestaAdmin)
admin.site.register(PosibleRespuesta, PosibleRespuestaAdmin)

