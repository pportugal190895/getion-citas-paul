import datetime
from _lsprof import profiler_entry

from servicios.constants import DIAS
from servicios.models import Profesional, Horario
from reservas.models import Reserva


def get_str_dia(dia):
    """
    Funcion que transforma el str de dias
    Ejemplo: 'lu' return 'Lunes' y viceversa

    :param dia: Str nombre del dia o diminutivo

    :return: Str nombre del dia o diminutivo o vacio
    """
    for d in DIAS:
        if d[0] == dia:
            return d[1]
        elif d[1] == dia:
            return d[0]
    return ''


class HorarioPorHora(object):
    """
    Clase encargada de obtener las horas y sus precios correspondientes

    :cvar pk: Integer con el pk del Profesional
    :cvar dia: String con el dia mapeable de DIAS
    """

    def __init__(self, pk, dia):
        self.pk = pk
        self.dia = dia

    def get_horas(self):
        """
        Funcion que recorre a traves de todas las horas entre la apertura y el
        cierre de la empresa, luego obtiene todos los horarios con precios
        especiales y asigna los precios por hora, de no tener un horario
        especial, saca el precio base del Profesional

        :return: List con horarios y sus precios
        """
        profesional = Profesional.objects.get(id=self.pk)
        hora_apertura = profesional.empresa.hora_apertura.hour
        hora_cierre = profesional.empresa.hora_cierre.hour
        horarios_precios = Horario.objects.filter(profesional=profesional, dia=self.dia)
        horario_por_hora = list()
        for hora in range(hora_apertura, hora_cierre):
            agregado = False
            for horario_precio in horarios_precios:
                if horario_precio.hora_inicio.hour <= hora < (
                        horario_precio.hora_fin.hour):
                    horario_por_hora.append(
                        (hora, hora + 1, horario_precio.precio))
                    agregado = True
            if not agregado:
                horario_por_hora.append((hora, hora + 1, profesional.precio))
        return horario_por_hora


class HorarioPorFecha(object):
    """
    Clase que obtiene los profesionales disponibles por horas en el intervalo de horas
    de apertura y cierre del local

    :cvar empresa: Empresa instancia
    :cvar fecha: datetime.date que nos dira la fecha que hara el llenado de los profesionales
    """

    def __init__(self, empresa, fecha):
        self.empresa = empresa
        self.fecha = fecha

    def get_horas(self):
        """
        Funcion que busca a partir de la hora de inicio y fin de la empresa,
        busca cuantos profesionales hay disponibles por horas

        :return: List con los horarios y el numero de profesionales disponibles
        """
        profesionales = Profesional.objects.filter(empresa=self.empresa)
        hora_apertura = self.empresa.hora_apertura.hour
        hora_cierre = self.empresa.hora_cierre.hour
        horario_por_hora = list()
        for hora in range(hora_apertura, hora_cierre):
            time = datetime.time(hour=hora)
            time_fin = (
                    datetime.datetime.combine(
                        datetime.date(1, 1, 1), time)
                    + datetime.timedelta(hours=1)
            ).time()
            # Filtra el numero de profesionales disponible menos los
            # profesionales reservados en un periodo de tiempo hora por hora
            hora = dict(
                inicio=hora, fin=hora + 1,
                profesionales_disponibles=profesionales.exclude(id__in=(Reserva.objects.filter(
                    profesional__in=profesionales,
                    inicio_hora_reserva__lte=time,
                    fin_hora_reserva__gte=time_fin,
                    fecha=self.fecha).values('profesional')))
            )
            horario_por_hora.append(hora)
        return horario_por_hora


