from rest_framework import serializers

from customauth.models import PersonalizacionPagina
from reservas.utils import PreciosPorReserva
from servicios.models import Empresa, Profesional


class ProfesionalSerializer(serializers.ModelSerializer):
    """
    Serializer para el modelo Profesional
    """
    class Meta:
        model = Profesional
        fields = ('nombres', 'apellidos', 'precio', )


class EmpresaSerializer(serializers.ModelSerializer):
    """
    Serializer para el modelo de Empresa
    """
    class Meta:
        model = Empresa
        fields = ('nombre', 'ruc', 'direccion', 'telefono', 'representante')


class EmpresaFavoritaSerializer(serializers.ModelSerializer):
    """
    Serializer para el modelo de EmpresaFavorita
    """
    class Meta:
        model = Empresa
        fields = ('id', 'nombre', 'direccion')


class SimpleEmpresaSerializer(serializers.ModelSerializer):
    """
    Serializer para el modelo de Empresa
    """
    class Meta:
        model = Empresa
        fields = ('id', 'nombre', 'direccion')


class DetalleEmpresaSerializer(serializers.ModelSerializer):
    """
    Serializer para el detalle de una empresa con los campos nombre, direccion,
    distrito, provincia y departamento.
    {
        "nombre": "EmpresaA.srl"
        "direccion": "Calle a empresa nro 1"
        "distrito": "arequipa"
        "provincia": "arequipa"
        "departamento": "arequipa"
    }
    """
    distrito = serializers.SerializerMethodField()
    provincia = serializers.SerializerMethodField()
    departamento = serializers.SerializerMethodField()

    def get_distrito(self, obj):
        return obj.distrito.nombre

    def get_provincia(self, obj):
        return obj.distrito.provincia.nombre

    def get_departamento(self, obj):
        return obj.distrito.provincia.departamento.nombre

    class Meta:
        model = Empresa
        fields = ('nombre', 'direccion', 'distrito', 'provincia', 'departamento')


class EmpresaBusquedaSerializer(serializers.ModelSerializer):
    """
    Serializer para las canchas disponibles de la busqueda
    [{
        "id": 1,
        "nombre": "Canchas el Locaso",
        "direccion": "Urb Los glandiolos 123",
        "icono": "http://localhost:8000/media/media/configuraciones/Cartman-Cop-head-icon_mbR6UVA.png"
    },{...}]
    """
    icono = serializers.SerializerMethodField()

    def get_icono(self, obj):
        request = self.context.get('request')
        personalizacion = PersonalizacionPagina.objects.filter(empresa=obj).first()
        if personalizacion:
            return request.build_absolute_uri(personalizacion.icono_logo_empresa.url)
        return None

    class Meta:
        model = Empresa
        fields = ('id', 'nombre', 'direccion', 'icono', )


class ProfesionalBusquedaSerializer(serializers.ModelSerializer):
    """
    Serializer para listar las canchas por Empresa.
    [{
        "id": 1,
        "identificador": "profesional nro 1"
        "foto_cancha": "http://localhost:8000/media/media/FOTO-CANCHA.png"
        "precio_total": 50.00
    }, {...}]
    """
    precio_total = serializers.SerializerMethodField()
    foto_profesional = serializers.SerializerMethodField()

    def get_precio_total(self, obj):
        """
        Metodo usado para obtener el precio del Profesional en los horarios pasados.

        :param obj: Objeto Profesional.

        :return: Float con el precio en ese horario.
        """
        hora_inicio = self.context.get('hora_inicio')
        hora_fin = self.context.get('hora_fin')
        fecha = self.context.get('fecha')
        return PreciosPorReserva(hora_inicio, hora_fin, obj, fecha).get_precio()

    class Meta:
        model = Profesional
        fields = ('id', 'identificador', 'foto', 'precio_total')


class FotosCanchaSerializer(serializers.ModelSerializer):
    """
    Serializer para obtener la lista de fotos de un Profesional
    {
        "fotos_cancha":[
            "http://localhost:8000/media/media/FOTO1.png",
            "http://localhost:8000/media/media/FOTO2.png"
        ]
    }
    """
    foto_profesional = serializers.SerializerMethodField()

    class Meta:
        model = Profesional
        fields = ('foto_profesional',)

