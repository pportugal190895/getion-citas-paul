from _lsprof import profiler_entry

from django.core.validators import MaxValueValidator
from django.db import models
from django.db.models import Avg

from servicios.constants import DIAS


class Empresa(models.Model):
    """
    Modelo de los objetos de la Empresa

    :cvar nombre: CharField que contiene el nombre de la empresa
    :cvar ruc: Charfield que contiene el numero de ruc de minimo y maximo 11
        digitos
    :cvar direccion: Charfield contiene la direccion de la empresa
    :cvar telefono: Charfield  contiene el numero de telefono de la empresa
    :cvar representante: Charfield contiene el Usuario representante
    :cvar tipo_servicio: ForeignKey contiene el Tipo de servicio de la empresa
    :cvar hora_apertura: IntegerField guarda la hora de apertura de la empresa
    :cvar hora_cierre: IntegerField guarda la hora de cierre de la empresa
    :cvar distrito: ForeignKey contiene el Tipo de distrito de la empresa
    """
    nombre = models.CharField(max_length=129)
    ruc = models.CharField(max_length=11)
    direccion = models.CharField(max_length=200)
    telefono = models.CharField(max_length=9)
    representante = models.CharField(max_length=128)
    hora_apertura = models.TimeField()
    hora_cierre = models.TimeField()
    distrito = models.ForeignKey(#Todo: URGENT !!!!! before deploying this change null = false and blank false too
        'Distrito', null=True, blank=True, on_delete=models.CASCADE)
    tipo_servicio = models.ForeignKey(
        'TipoServicio', on_delete=models.CASCADE, null=True, blank=True)
    logotipo = models.ImageField(
        upload_to='media/company-photos/', null=True, blank=True)

    def __str__(self):
        """
        Funcion que retorna el objeto al ser llamado, este conforma el nombre y
        ruc

        :return: Str que contiene el nombre y ruc de la empresa
        """
        return "{nombre}, {ruc}".format(nombre=self.nombre, ruc=self.ruc)

    class Meta:
        verbose_name_plural = 'Empresa'

    @property
    def profesional(self):
        return Profesional.objects.filter(empresa=self)


class TipoServicio(models.Model):
    """
    Modelo que almacenara los diferentes tipos de servicios

    :cvar tipo_servicio: Charfield que contiene el tipo servicio
    :cvar imagen: ImageField icono representativo para cada servicio.
    :cvar slug: CharField que almacena el slug del tipo de servicio
    """
    tipo_servicio = models.CharField(max_length=64, unique=True)
    imagen = models.ImageField(upload_to='media/', null='true', blank='true')
    slug = models.SlugField(max_length=64, unique=True)

    def __str__(self):
        return self.tipo_servicio


class Profesional(models.Model):
    """
    Modelo que almacenara los datos de un Profesional perteneciente a una empresa

    :cvar nombres: CharField que guarda el nombre del profesional
    :cvar apellido_paterno: CharField que guarda el apellido paterno del
        profesional
    :cvar apellido_materno: CharField que guarda el apellido materno del
        profesional
    :cvar precio: FloatField que almacena el precio base del Profesional
    :cvar empresa: ForeignKey que almacena a que empresa pertenece el Profesional
    :cvar descripcion: TextField que contiene la descripcion del profesional
    """
    nombres = models.CharField(max_length=200)
    apellido_paterno = models.CharField(max_length=256)
    apellido_materno = models.CharField(max_length=256)
    precio = models.FloatField(verbose_name='Precio base')
    empresa = models.ForeignKey(
        Empresa, on_delete=models.CASCADE, null=True)
    descripcion = models.TextField(blank=True)
    puntaje = models.FloatField(default=0)
    foto = models.ImageField(
        upload_to='media/professional-photos', null=True, blank=True)

    def __str__(self):
        return "{}, {}".format(self.nombres, self.precio)

    class Meta:
        verbose_name_plural = 'Profesional'

    def calcular_puntaje(self):
        from reservas.models import Resena
        resenas = Resena.objects.filter(reserva__profesional=self)
        if resenas.exists():
            return round(resenas.aggregate(Avg('puntaje'))['puntaje__avg'])
        else:
            return 0

    @property
    def redondear_puntaje(self):
        return round(self.puntaje)


class Horario(models.Model):
    """
    Modelo que almacenara los horarios a trabajar por las empresas y los precios
    dependiendo de la hora.

    :cvar hora_inicio: TimeField que almacena la hora de inicio del horario.
    :cvar hora_fin: TimeField que almacena la hora finalizada del horario.
    :cvar precio: FloatField que almacena el precio del horario especifico.
    :cvar dia: CharField que almacena el dia de la semana del horario.
    :cvar profesional: ForeignKey Que almacena el Profesional a la que pertenece este
        horario.
    """
    hora_inicio = models.TimeField()
    hora_fin = models.TimeField()
    precio = models.FloatField()
    dia = models.CharField(max_length=2, choices=DIAS)
    profesional = models.ForeignKey(
        Profesional, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return "{}, {}, {} - {}".format(
            self.profesional.nombres, self.hora_inicio, self.hora_fin,
            self.precio
        )


class Departamento(models.Model):
    """
    Modelo que almacena los datos de un Departamento.

    :cvar nombre: Charfield que guarda el nombre del Departamento.
    """
    nombre = models.CharField(max_length=64)

    def __str__(self):
        return self.nombre


class Provincia(models.Model):
    """
    Modelo que almacena los datos de una provincia.

    :cvar departamento: ForeignKey que apunta a un Deparatamento.
    :cvar nombre: Charfield que guarda el nombre de la provincia.
    """
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=64)

    class Meta:
        unique_together = ('nombre', 'departamento')

    def __str__(self):
        return self.nombre


class Distrito(models.Model):
    """
    Modelo que almacena los datos de una provincia.

    :cvar nombre: Charfield que guarda el nombre del distrito.
    :cvar provincia: ForeingKey que apunta a una Provincia.
    """
    provincia = models.ForeignKey(Provincia, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=64)

    class Meta:
        unique_together = ('nombre', 'provincia')

    def __str__(self):
        return self.nombre

    @property
    def nombre_provincia(self):
        return self.provincia.nombre

    @property
    def nombre_departamento(self):
        return self.provincia.departamento.nombre

