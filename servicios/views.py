import datetime

from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View
from django.views.generic import (
    ListView,
    UpdateView,
    CreateView,
    DeleteView,
    FormView
)

from servicios.constants import DIAS
from servicios.forms import HorarioCreateForm, EmpresaForm
from servicios.models import (
    Profesional,
    Horario,
    TipoServicio,
    Empresa,
    Provincia,
    Distrito,
    Departamento,
)
from servicios.utils import HorarioPorHora, get_str_dia


class ProfesionalListView(ListView):
    """
    ListView que retorna las canchas correspondientes al usuario
    """
    queryset = Profesional.objects.all()
    context_object_name = 'profesionales'
    template_name = 'profesional/profesional_list.html'

    def get_queryset(self):
        """
        Funcion que obtiene las canchas que pertenecen a la empresa del usuario

        :return: Profesional queryset con las canchas de la empresa
        """
        self.queryset = Profesional.objects.filter(
            empresa=self.request.user.empresa)
        return super(ProfesionalListView, self).get_queryset()


class ProfesionalUpdateView(UpdateView):
    """
    UpdateView que actualiza el objeto profesional
    """
    model = Profesional
    fields = [
        'nombres',
        'apellido_paterno',
        'apellido_materno',
        'precio',
        'descripcion',
    ]
    template_name = 'profesional/profesional_update.html'

    def get_success_url(self):
        """
        Funcion que redirecciona, una vez haya sido guardado el objeto con exito

        :return: url por nombre que redirecciona a la vista de canchas
        """
        return reverse('servicios_profesionales_list')


class ProfesionalCreateView(CreateView):
    """
    CreateView que crea canchas para el usuario y la empresa relacionada
    """
    model = Profesional
    fields = [
        'nombres',
        'apellido_paterno',
        'apellido_materno',
        'precio',
        'descripcion',
    ]
    template_name = 'profesional/profesional_create.html'

    def get_success_url(self):
        """
        Funcion que redirecciona una vez haya sido guardado el objeto con exito

        :return: url por nombre que redirecciona a la vista de canchas
        """
        return reverse('servicios_profesionales_list')

    def form_valid(self, form):
        """
        Funcion que se llama una vez el formulario sea valido

        :param form: Obj que contiene la informacion a guardar

        :return: HttpResponseRedirect que llama a la funcion get_success_url
        """
        object = form.save()
        object.empresa = self.request.user.empresa
        object.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_form(self):
        form = super(ProfesionalCreateView, self).get_form()
        form.fields['descripcion'].widget.attrs['placeholder'] = (
            "Ej: Juan Doe es un profesional con mas de 20 años en el rubro de "
            "con especialidad en ..."
        )
        return form


class HorariosDiasPorSemanaView(View):
    """
    Vista encargada de la lógica de lapagina de los botones de los dias de la
    semana.

    :cvar template_name: nombre del template a renderizar.
    """
    template_name = 'horario/dias_por_semana.html'

    def get(self, request, pk):
        """
        Metodo get usado para renderizar la pagina.

        :param request: Parametro que guarda la infomacion de la pagina.
        :param pk: Id de un objeto Profesional.

        :return: Metodo de renderizado con los datos.
        """
        context = {
            'profesional_id': pk,
            'dias': DIAS
        }
        return render(request, self.template_name, context)


class HorarioListFormView(FormView):
    """
    Vista heredada de ListView y CreateView usada Para implementar la logica de
    la pagina de listado de horarios dependiendo del dia y tambien el formulario
    de guardado del mismo.

    :cvar template_name: Nombre del template a renderizar.
    :cvar model: Nombre del modelo que se queire listar.
    :cvar fields:
    :cvar cancha:
    :cvar cancha_dia:
    """
    template_name = 'horario/horario_dia_list.html'
    form_class = HorarioCreateForm

    profesional = None
    profesional_dia = None

    def get(self, request, profesional_pk, dia, *args, **kwargs):
        """
        Metodo get usado para adicionar

        :param request: Parametro que guarda la informacion de la pagina.
        :param cancha_pk: Int id del objeto profesional.
        :param dia: Str que guarda el dia de la semana del horario.
        :return: Renderizacion de la pagina.
        """
        self.profesional = Profesional.objects.get(id=profesional_pk)
        self.profesional_dia = dia
        return super(HorarioListFormView, self).get(request, *args, **kwargs)

    def post(self, request, profesional_pk, dia, *args, **kwargs):
        """
        Metodo post que redirecciona a la pagina de creacion del horario.

        :param request: Parametro que guarda la informacion de la pagina.
        :param cancha_pk: Int id del objeto profesional.
        :param dia: Str que guarda el dia de la semana del horario.
        :return: Renderizacion hacia la pagina de creacion y validacion.
        """
        hora_inicio = request.POST.get('hora_inicio')
        hora_fin = request.POST.get('hora_fin')
        precio = request.POST.get('precio')
        kwargs = {
            'profesional_pk': profesional_pk,
            'dia': dia,
            'hora_inicio': hora_inicio,
            'hora_fin': hora_fin,
            'precio': precio
        }
        return HttpResponseRedirect(
            reverse('servicios-profesionales-horario-create', kwargs=kwargs))


    def get_context_data(self, **kwargs):
        """
        Metodo usado para obtener el contexto de la pagina y actualizarlo.

        :return: Nuevo contexto actualizado.
        """
        context = super(HorarioListFormView, self).get_context_data()
        context['profesional'] = self.profesional
        context['dia'] = get_str_dia(self.profesional_dia)
        context['dia_str'] = self.profesional_dia
        context['horas'] = HorarioPorHora(
            self.profesional.id, self.profesional_dia).get_horas()
        return context


class HorarioCreateView(CreateView):
    """
    Vista CreateView que renderisa  la pagina de confirmacion de creacion de un
    nuevo horario.

    :cvar model: el modelo a usarse.
    :cvar fields: los campos a renderizare.
    :cvar profesional: variable que guardara el objeto profesional
    :cvar cancha_dia: variable que guardara el dia del horario
    :cvar horarios_previos_afectados: variable que guardara los horarios
        afectados con anterioridad del nuevo horario.
    :cvar horarios_posterior_afectados: variable que guardara los horarios con
        posterioridad del nuevo creado
    :cvar horarios_por_borrar: variable que guardara los horarios que esten
        dentro del rango de horas del nuevo creado
    """
    template_name = 'horario/horario_create.html'
    model = Horario
    fields = []
    horarios_previos_afectados = None
    horarios_posterior_afectados = None
    horarios_por_borrar = None

    def __init__(self):
        self.errors = []

    def get(self, request, profesional_pk, dia, hora_inicio, hora_fin, precio,
            *args, **kwargs):
        """
        Metdo get que actualiza el contexto de los horarios afectados.

        :param request: parametro que guarda la informacion de la pagina.
        :param cancha_pk: parametro que guarda el id de un Profesional.
        :param dia: parametro que guarda el dia de un horario
        :param precio: precio de un horario.

        :return: Metodo de renderizado con el contexto actualizado.
        """
        self.profesional = Profesional.objects.get(id=profesional_pk)
        self.dia = dia
        self.hora_inicio = hora_inicio
        self.hora_fin = hora_fin
        self.precio = precio
        contexto = self.get_context_data()
        contexto['errors'] = self.validar(hora_inicio, hora_fin)
        return render(request, 'horario/horario_create.html', contexto)

    def post(self, request, profesional_pk, dia, hora_inicio, hora_fin, precio,
             *args, **kwargs):
        """
        Metodo post que actualiza o borra horarios segun el metodo validar.

        :param request: parametro que guarda la informacion de la pagina.
        :param profesional_pk: parametro que guarda el id de un Profesional.
        :param dia: parametro que guarda el dia de un horario
        :param precio: precio de un horario.
        :return: Guardado del nuevo horario.
        """
        self.profesional = Profesional.objects.get(id=profesional_pk)
        self.dia = dia
        self.hora_inicio = hora_inicio
        self.hora_fin = hora_fin
        self.precio = precio
        self.validar(hora_inicio, hora_fin)
        # actualiza los horarios afectados previos
        self.horarios_previos_afectados.update(hora_fin=datetime.time(
            hour=hora_inicio)) if self.horarios_previos_afectados.exists() else None
        # actualiza los horarios
        self.horarios_posterior_afectados.update(hora_inicio=datetime.time(
            hour=hora_fin)) if self.horarios_posterior_afectados.exists() else None
        # borra los horarios afectados dentro del rango del nuevo horario
        self.horarios_por_borrar.delete() if self.horarios_por_borrar.exists() else None
        return super(HorarioCreateView, self).post(request, *args, **kwargs)

    def validar(self, hora_inicio, hora_fin):
        """
        Metodo que valida que horarios creados anteriormente han sido afectados
        en la creacion del este.

        :return: Variable que contiene la lista de horarios afectados.
        """
        hora_inicio = datetime.time(hour=hora_inicio)
        hora_fin = datetime.time(hour=hora_fin)
        horarios_previos_afectados = Horario.objects.filter(
            hora_fin__gt=hora_inicio, hora_inicio__lt=hora_inicio,
            profesional=self.profesional, dia=self.dia)
        horarios_posterior_afectados = Horario.objects.filter(
            hora_fin__gt=hora_fin, hora_inicio__lt=hora_fin,
            profesional=self.profesional, dia=self.dia)
        horarios_por_borrar = Horario.objects.filter(
            hora_inicio__gte=hora_inicio, hora_fin__lte=hora_fin,
            profesional=self.profesional, dia=self.dia)
        self.horarios_previos_afectados = horarios_previos_afectados
        self.horarios_posterior_afectados = horarios_posterior_afectados
        self.horarios_por_borrar = horarios_por_borrar
        self.errors.extend(horarios_previos_afectados)
        self.errors.extend(horarios_posterior_afectados)
        self.errors.extend(horarios_por_borrar)
        return self.errors

    def form_valid(self, form):
        """
        Metodo usado para validar el formulario.

        :param form: Formulario que se usa en la pagina.

        :return: Actualizacion del formulario.
        """
        form.instance.profesional = self.profesional
        form.instance.dia = self.dia
        form.instance.hora_inicio = datetime.time(hour=self.hora_inicio)
        form.instance.hora_fin = datetime.time(hour=self.hora_fin)
        form.instance.precio = self.precio
        return super(HorarioCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        """
        Metodo que actualiza el contexto.

        :return: contexto.
        """
        contexto = {
            'profesional': self.profesional,
            'dia': self.dia,
            'hora_inicio': self.hora_inicio,
            'hora_fin': self.hora_fin,
            'precio': self.precio
        }
        return contexto

    def get_success_url(self):
        """
        Metodo usado para redireccionar una ve el formulario no este valido.

        :return: Metodo usado para redireccionar hacia la lista de horarios.
        """
        kwargs = {'profesional_pk': self.profesional.id, 'dia': self.dia}
        return reverse('servicios-profesionales-horarios-list', kwargs=kwargs)


class ListarHorariosView(View):
    """
    Vista generica que renderisa la lista de los horarios.
    """
    template_name = 'horario/horario_delete.html'

    def get(self, request, profesional_pk, dia):
        """
        Metodo get que define el contexto de la pagina como la lista de horarios

        :param request: parametro que guarda la informacion de la pagina.
        :param profesional_pk: parameto que guarda el id del Profesional.
        :param dia: parametro que guarda el dia de las reservas.
        :return: metodo de redireccion a la lista de horarios.
        """
        profesional = Profesional.objects.get(id=profesional_pk)
        horarios = Horario.objects.filter(profesional=profesional, dia=dia)
        contexto = {
            'horarios': horarios, 'profesional': profesional, 'dia': dia
        }
        return render(request, 'horario/horario_delete.html', contexto)


class HorarioDeleteView(DeleteView):
    """
    Vista DeleteView que elimina un horario previo seleccionado.
    :cvar model: objeto que guarda el modelo a ser borrado.
    """
    model = Horario

    def delete(self, request, *args, **kwargs):
        """
        Funcion que sobrecarga el delete de Horario DeleteView para que pueda
        redireccionar a la lista de horarios por hora.

        :param request: Obj que contiene la metadata del request.
        :param kwargs: Dict que contiene los known arguments pasados en el url
            del delete

        :return: HttpResponseRedirect hacia la lista de horarios
        """
        profesional_pk = self.kwargs['profesional_pk']
        dia = self.kwargs['dia']
        horario_id = self.kwargs['pk']
        Horario.objects.get(id=horario_id).delete()
        kwargs = {'profesional_pk': profesional_pk, 'dia': dia}
        return HttpResponseRedirect(
            reverse('servicios-profesionales-horarios-list', kwargs=kwargs))


class ServiciosListView(ListView):
    """
    Vista generica del tipo ListView encargada de listar los tipos de servicios.
    """
    model = TipoServicio
    template_name = 'servicios_list.html'
    queryset = TipoServicio.objects.all()
    context_object_name = "servicios"


class EmpresaUpdateView(UpdateView):
    """
    Vista que renderizar la pagina de registro de Empresa.
    link: http://localhost:8000/servicios/empresa-editar/ID/
    """
    model = Empresa
    form_class = EmpresaForm
    template_name = 'empresa_edit.html'

    def get_success_url(self):
        """
        Metodo get_success_url que redireciona a la vista home de empresas

        :return: Metodo reverse usado para redireccionar
        """
        return reverse('home-enterprise-view')
