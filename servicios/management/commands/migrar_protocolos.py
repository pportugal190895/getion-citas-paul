import os

from django.conf import settings
from django.core.files import File
from django.core.management import BaseCommand
from django.utils.text import slugify

from openpyxl import load_workbook
from reservas.models import Protocolo, Pregunta, PosibleRespuesta
from servicios.models import TipoServicio


class Command(BaseCommand):

    help = "El archivo debe tener el nombre tipos_servicio_protocolos con la extención .xlsx"

    FILE_DEFAULT_NAME = 'sys_files/tipos_servicio_protocolos.xlsx'

    def add_arguments(self, parser):
        """
        Metodo usado para añadirle un argumento al comando implementado.
        """
        parser.add_argument(
            '--file', type=str, help="indica la ruta del archivo"
        )

    def handle(self, *args, **kwargs):
        """
        Metodo que lee el archivo e implementa la creacion de protocolos de los
        tipos de servicio.
        """
        file = kwargs['file']
        if not file:
            file = os.path.join(
                getattr(settings, 'STATICFILES_DIRS', None)[0],
                self.FILE_DEFAULT_NAME
            )
        try:
            # intenta abrir el archivo con su ruta "file"
            wb = load_workbook(file)
            worksheet = wb['Protocolos']
            for row in worksheet.iter_rows(min_row=2):
                # recorre todas las celdas del archivo obviando la primera.
                if row[0].value:
                    # consulta la columna de Tipo servico y la guarda.
                    tipo_servicio = self.crear_tipo_servicio(row)
                if row[1].value:
                    # consulta la columna del nombre de protocolo y lo guarda.
                    if row[1].value == 'profesional':
                        protocolo, _ = Protocolo.objects.get_or_create(
                            texto=tipo_servicio.tipo_servicio,
                            tipo_servicio=tipo_servicio,
                            es_para_profesional=True
                        )
                    else:
                        protocolo, _ = Protocolo.objects.get_or_create(
                            texto=tipo_servicio.tipo_servicio,
                            tipo_servicio=tipo_servicio,
                            es_para_profesional=False
                        )
                if row[2].value:
                    # consuta el tipo de pregunta para guardar la pregunta.
                    tipo_pregunta = row[2].value
                if row[3].value:
                    # consulta la pregunta y la guarda.
                    pregunta, _ = Pregunta.objects.get_or_create(
                        texto=row[3].value,
                        protocolo=protocolo,
                        question_type=tipo_pregunta
                    )
                for ps in row[4:]:
                    # recorre las posibles respuestas de la pregunta.
                    if ps.value:
                        PosibleRespuesta.objects.get_or_create(
                            texto=ps.value.split(':')[0],
                            weight=ps.value.split(':')[1],
                            pregunta=pregunta
                        )
            self.stdout.write("migracion finalizada con exito")
        except Exception as e:
            # ocurre si el archivo no se puede abrir o la ruta no existe
            self.stdout.write(
                "error - la direccion del archivo es incorrecta. " + str(e))

    def crear_tipo_servicio(self, row):
        """
        Metodo que creara el tipo de servicio y su imagen.

        :param row: fila del documento donde se encuentra el nombre.

        :return: TipoServicio Model instance.
        """

        rubro_images = getattr(settings, 'STATICFILES_DIRS', None)[0]
        tipo_servicio, _ = TipoServicio.objects.get_or_create(
            tipo_servicio=row[0].value)
        slug = slugify(tipo_servicio.tipo_servicio)
        tipo_servicio.slug = slug
        imagen = os.path.join(
            rubro_images, (f"static/tipo_servicios_icons/{slug}.png"))
        try:
            tipo_servicio.imagen.save(
                f"{slug}.png",
                File(open(imagen, 'rb')))
        except Exception as e:
            self.stdout.write(f"No se asigno una imagen para {slug}")
        tipo_servicio.save()
        return tipo_servicio
