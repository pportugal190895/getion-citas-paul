import os
from string import digits

from django.conf import settings
from django.core.management import BaseCommand

from openpyxl import load_workbook

from servicios.models import Departamento, Provincia, Distrito


class Command(BaseCommand):
    """
    Comando usado para migrar los datos de ubigeo referente a los departamentos
    provincias y distritos.

    url rptUbigeo: http://webinei.inei.gob.pe:8080/sisconcode/proyecto/index.htm?proyectoTitulo=UBIGEO&proyectoId=3

    Comando: "python manage.py migrar_ubigeo FILE_PATH.xlsx"
    """

    help = "El archivo debe tener el nombre rptUbigeo con la extención .xlsx"

    FILE_DEFAULT_NAME = 'sys_files/rptUbigeo.xlsx'

    def add_arguments(self, parser):
        """
        Metodo usado para añadirle un argumento al comando implementado.
        """
        parser.add_argument('--file', type=str, help="indica la direccion del archivo")

    def handle(self, *args, **kwargs):
        """
        Metodo usado para implementar el comando.
        """
        file = kwargs['file']
        if not file:
            file = os.path.join(
                getattr(settings, 'STATICFILES_DIRS', None),
                self.FILE_DEFAULT_NAME
            )
        try:
            #intenta abrir el archivo con su ruta "file"
            wb = load_workbook(file)
            ws = wb['ubicacionGeografica']
            for row in list(ws.rows):
                if not self.is_blank(row):
                    #if que consulta si la fila del archivo esta en blanco
                    if not self.is_title(row):
                        #if que consulta si la fila del archivo contiene un titulo
                        if row[4].value.lstrip() != '' and row[5].value.lstrip() != '':
                            #if que consulta si las dos primeras columnas no estan en blanco
                            nombre_departamento = self.clean_str_noise(row[1].value)
                            departamento, _ = Departamento.objects.get_or_create(
                                nombre=nombre_departamento
                            )
                            nombre_provincia = self.clean_str_noise(row[4].value)
                            provincia, _ = Provincia.objects.get_or_create(
                                departamento=departamento, nombre=nombre_provincia,
                            )
                            nombre_distrito = self.clean_str_noise(row[5].value)
                            Distrito.objects.get_or_create(
                                provincia=provincia, nombre=nombre_distrito,
                            )
            self.stdout.write("migracion finalizada con exito")
        except Exception as e:
            #ocurre si el archivo no se puede abrir o la ruta no existe
            self.stdout.write("error - la direccion del archivo es incorrecta. " + str(e))

    def is_blank(self, row):
        """
        Metodo que consulta si row (fila) esta en blanco.

        :param row: la fila del archivo.
        :return: Bool
        """
        if not row[1].value and not row[4].value and not row[5].value:
            #Retornar True, si las columnas 1,4 y 5 estan en blanco.
            return True
        return False

    def is_title(self, row):
        """
        Metodo que consulta si row (fila) esta en mayusculas, por que el titulo
        esta siempre en mayusculas.

        :param row: la fila del archivo.
        :return: Bool
        """
        if row[1].value.isupper() and row[4].value.isupper() and (
                row[5].value.isupper()):
            #Retorna True, si las columnas 1,4 y 5 estan en mayusculas
            return True
        return False

    def clean_str_noise(self, value):
        """
        Metodo que quita numeros del nombre del value (departamento, provincia o
        distrito)

        :param value: Str con el nombre de un departamento, provicia o distrito
        :return: variable "value" con su nombre actualizado.
        """
        remove_digits = str.maketrans('', '', digits)
        value = value.translate(remove_digits).lstrip()
        return value
