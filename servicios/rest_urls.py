from django.conf.urls import url
from django.urls import include

from rest_framework.routers import DefaultRouter

from servicios.rest_views import (
    EmpresaViewSet,
    ProfesionalViewSet,
    EmpresaDetalleViewSet
)


router = DefaultRouter()
router.register(r'empresas', EmpresaViewSet)
router.register(r'canchas', ProfesionalViewSet)
router.register(r'empresa-profesional/detalle', EmpresaDetalleViewSet)

urlpatterns = [
    url(r'reserva-canchas/', include(router.urls))
]