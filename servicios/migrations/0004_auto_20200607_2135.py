# Generated by Django 3.0.6 on 2020-06-08 02:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('servicios', '0003_profesional_puntaje'),
    ]

    operations = [
        migrations.AddField(
            model_name='tiposervicio',
            name='slug',
            field=models.SlugField(default=11, max_length=64),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='tiposervicio',
            name='tipo_servicio',
            field=models.CharField(max_length=64, unique=True),
        ),
    ]
