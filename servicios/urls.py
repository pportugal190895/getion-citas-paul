from django.urls import path

from servicios.views import (
    ProfesionalListView,
    ProfesionalUpdateView,
    ProfesionalCreateView,
    HorariosDiasPorSemanaView,
    HorarioListFormView,
    HorarioCreateView,
    ListarHorariosView,
    HorarioDeleteView,
    ServiciosListView,
    EmpresaUpdateView,
)


urlpatterns = [
    path('profesionales/', ProfesionalListView.as_view(),
         name='servicios_profesionales_list'),
    path('profesional-editar/<int:pk>', ProfesionalUpdateView.as_view(),
         name='servicios-profesionales-edit'),
    path('profesional-crear/', ProfesionalCreateView.as_view(),
         name='servicios-profesionales-create'),
    path('horario-profesional/<int:pk>', HorariosDiasPorSemanaView.as_view(),
         name='servicios-profesionales-dias-por-semana'),
    path('horas/<int:profesional_pk>/<slug:dia>/', HorarioListFormView.as_view(),
         name='servicios-profesionales-horarios-list'),
    path('horario-create/<int:profesional_pk>/<slug:dia>/<int:hora_inicio>/<int:hora_fin>/<int:precio>/',
         HorarioCreateView.as_view(),
         name='servicios-profesionales-horario-create'),
    path('horario-list/<int:profesional_pk>/<slug:dia>/',
         ListarHorariosView.as_view(),
         name='servicios-profesionales-horario-list-to-delete'),
    path('horario-delete/<int:profesional_pk>/<slug:dia>/<int:pk>',
         HorarioDeleteView.as_view(),
         name='servicios-profesionales-horario-delete'),
    path('servicios-list', ServiciosListView.as_view(), name='servicios-list'),
    path('empresa/editar/<int:pk>/', EmpresaUpdateView.as_view(),
         name='servicios-empresa-edit'),
]

