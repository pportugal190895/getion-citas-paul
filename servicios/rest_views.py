import datetime

from rest_framework import viewsets, status, mixins
from rest_framework.decorators import action
from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response

from customauth.models import EmpresaFavorita
from customauth.permissions import EndUserIsAuthenticated
from customauth.utils import (
    get_empresas_disponibles,
    DisponibilidadProfesionales,
)
from servicios.models import Profesional, Empresa
from servicios.serializers import (
    ProfesionalSerializer,
    EmpresaSerializer,
    SimpleEmpresaSerializer,
    EmpresaFavoritaSerializer,
    DetalleEmpresaSerializer,
    EmpresaBusquedaSerializer,
    ProfesionalBusquedaSerializer,
    FotosCanchaSerializer,
)


class EmpresaViewSet(viewsets.ModelViewSet):
    """
    ViewSet que listara todas las empresas de canchas
    """
    serializer_class = EmpresaSerializer
    queryset = Empresa.objects.all()
    permission_classes = (EndUserIsAuthenticated,)

    @action(
        detail=False, url_name='searchs-empresa-profesional',
        url_path='busqueda/(?P<fecha>\d{4}-\d{2}-\d{2})/(?P<hora_inicio>\d{2})/(?P<hora_fin>\d{2})')
    def busqueda_basica(self, request, fecha, hora_inicio, hora_fin=None):
        """
        Vista para hacer la busqueda

        :param request: Objecto que tiene la metadata del request
        :param fecha: Str que tiene la siguiente forma
            "1994-06-31"
        :param hora_inicio: Str con la hora de inicio ej. 23
        :param hora_fin: Str con la hora de fin ej. 22

        :return: Response con las empresas disponible
        """
        distrito = None
        provincia = 'Arequipa'
        departamento = None

        hora_inicio = datetime.time(int(hora_inicio))
        hora_fin = datetime.time(int(hora_fin))
        fecha = [int(i) for i in fecha.split("-")]
        fecha = datetime.date(fecha[0], fecha[1], fecha[2])
        empresas = get_empresas_disponibles(
            fecha, hora_inicio, hora_fin, distrito, provincia, departamento)
        serializer = EmpresaBusquedaSerializer(empresas, many=True, context={"request": request})

        return Response(data=serializer.data)

    @action(detail=False, url_name='empresa-favorita-list')
    def favoritas(self, request):
        """
        Funcion que retorna las empresas de profesional Favoritas de un usuario. GET
        http://localhost:8000/api/rest/rc/reserva-canchas/empresas/favoritas/

        :param request: Objeto que contiene la metadata del request.

        :return: Response con los datos del serializador.
        """
        empresas_favoritas = EmpresaFavorita.objects.filter(
            usuario=request.user)
        queryset = Empresa.objects.filter(
            empresafavorita__in=empresas_favoritas)
        serializer = EmpresaFavoritaSerializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=True, url_name='empresa-favorita-create-delete')
    def set_unset_favoritas(self, request, pk=None, *args, **kwargs):
        """
        Funcion que agrega o quita canchas favoritas a los usuarios. GET
        http://localhost:8000/api/rest/rc/reserva-canchas/empresas/ID-EMPRESA/set_unset_favoritas/

        :param request: Objeto que contiene la metadata del request
        :param pk: Int que contiene el id de la empresa

        :return: Response 200 o 201
        """
        empresa = Empresa.objects.get(id=pk)
        empresa_favorita = EmpresaFavorita.objects.filter(
            empresa=empresa, usuario=request.user)
        if empresa_favorita.exists():
            empresa_favorita.delete()
            return Response(data="Borrado", status=status.HTTP_200_OK)
        else:
            EmpresaFavorita.objects.create(empresa=empresa, usuario=request.user)
            return Response(data="Creado", status=status.HTTP_201_CREATED)

    @action(detail=True, url_name='detalle-profesional-por-empresa', methods=['get'],
            url_path='busqueda/canchas/(?P<hora_inicio>\d{2})/(?P<hora_fin>\d{2})/(?P<fecha>\d{4}-\d{2}-\d{2})')
    def busqueda_profesionales(
            self, request, hora_inicio, hora_fin, fecha, pk=None):
        """
        Metodo que implementa el servicio para obtener las canchas de una
        empresa y la primera foto de esta.
        Hacer get a esta direccion:
        http://localhost:8000/api/rest/rc/reserva-canchas/empresas/ID_EMPRESA/busqueda/canchas/HORA_INICIO/HORA_FIN/AÑO-MES-DIA/

        :param request: Objeto que contiene la metadata del request.
        :param hora_inicio: Str que contiene la hora de inicio para calcular el precio.
        :param hora_fin: Str que ocntiene la hora de fin para calcular el precio.
        :param fecha: Str con la fecha(2019-03-12).
        :param pk: int que contiene el id de la empresa.

        :return: Response con las canchas.
        """
        hora_inicio = datetime.time(int(hora_inicio))
        hora_fin = datetime.time(int(hora_fin))
        empresa = Empresa.objects.get(id=pk)
        fecha = [int(i) for i in fecha.split("-")]
        fecha = datetime.date(fecha[0], fecha[1], fecha[2])
        profesional = Profesional.objects.filter(empresa=empresa)
        # for que excluye las canchas no disponibles en ese horario y dia.
        for cancha in profesional:
            disponibilidad = DisponibilidadProfesionales(
                fecha, cancha, hora_inicio, hora_fin
            ).exists()
            if disponibilidad:
                profesional.exclude(cancha)
        serializer = ProfesionalBusquedaSerializer(
            profesional, many=True, context={
                'request': request,
                'hora_inicio': hora_inicio,
                'hora_fin': hora_fin,
                'fecha': fecha
            }
        )
        return Response(serializer.data)


class EmpresaDetalleViewSet(GenericViewSet, mixins.RetrieveModelMixin):
    """
    ViewSet que para obtener El detalle de una empresa con su direccion.
    Para hacer GET a esta vita usar el siguiente url:
    http://localhost:8000/api/rest/rc/reserva-canchas/empresa-cancha-detalle/ID_EMPRESA/
    """
    serializer_class = DetalleEmpresaSerializer
    queryset = Empresa.objects.all()


class ProfesionalViewSet(viewsets.ModelViewSet):
    """
    ViewSet que listara todas las canchas existentes
    """
    serializer_class = ProfesionalSerializer
    queryset = Profesional.objects.all()
    permission_classes = (EndUserIsAuthenticated,)

    @action(detail=True, methods=['get'])
    def fotos(self, request, pk=None):
        """
        Metodo usado para obtener las fotos de un Profesional.
        hacer GET a esta direccion:
        http://localhost:8000/api/rest/rc/reserva-canchas/canchas/ID-CANCHA/fotos/

        :param request: Objeto que contiene la metadata del request.
        :param pk: Int que contiene el id del Profesional.

        :return: Response con las fotos del Profesional
        """
        profesional = Profesional.objects.get(id=pk)
        serializer = FotosCanchaSerializer(
            profesional, many=False, context={'request': request}
        )
        return Response(serializer.data)

