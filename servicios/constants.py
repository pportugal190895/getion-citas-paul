DIAS = (
    ('lu', 'Lunes'),
    ('ma', 'Martes'),
    ('mi', 'Miercoles'),
    ('ju', 'Jueves'),
    ('vi', 'Viernes'),
    ('sa', 'Sabado'),
    ('do', 'Domingo')
)

MESES = (
    ('Ene', 'Enero'),
    ('Feb', 'Febrero'),
    ('Mar', 'Marzo'),
    ('Abr', 'Abril'),
    ('May', 'Mayo'),
    ('Jun', 'Junio'),
    ('Jul', 'Julio'),
    ('Ago', 'Agosto'),
    ('Sep', 'Septiembre'),
    ('Oct', 'Octubre'),
    ('Nov', 'Nobiembre'),
    ('Dic', 'Diciembre')
)

HORA_INICIO_GT_FIN = "El horario que usted escogió no esta disponible, la hora final debe ser mayor a la inicial."
HORA_INICIO_E_FIN = "El horario que usted escogió no esta disponible, por favor, las horas no deben ser iguales."
HORA_NO_DISPONIBLE = "El horario que usted escogió no esta disponible, por favor, <b>vuelva a revisar las horas.</b> O escoja un Profesional con el mismo horario mas abajo."
RESERVA_EXISTENTE = "Esta reserva ya existe porfavor modifique el horario y vuelva a calcular el precio."
