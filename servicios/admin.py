from django.contrib import admin
from django.contrib.admin import ModelAdmin
from nested_inline.admin import NestedModelAdmin

from reservas.admin import ProtocoloNestedStackedInline

from servicios.models import (
    Empresa,
    Profesional,
    Horario,
    TipoServicio,
)


class ProfesionalAdmin(ModelAdmin):
    """

    """

    list_display = (
        'nombres', 'apellido_paterno', 'apellido_materno', 'precio', 'empresa')


class HorarioAdmin(ModelAdmin):
    """

    """

    list_display = ('profesional', 'dia', 'hora_inicio', 'hora_fin', 'precio')


class TipoServicioAdmin(NestedModelAdmin):
    """
    ModelAdmin tipos servicio
    """

    list_display = ('tipo_servicio', 'imagen')
    inlines = (ProtocoloNestedStackedInline,)


admin.site.register(Empresa)
admin.site.register(Profesional, ProfesionalAdmin)
admin.site.register(Horario, HorarioAdmin)
admin.site.register(TipoServicio, TipoServicioAdmin)
