from django import forms
from django.forms import (
    ModelForm,
    ModelChoiceField,
)

from servicios.models import (
    Departamento,
    Provincia,
    Distrito,
    Empresa,
)

class HorarioCreateForm(forms.Form):
    """
    Formulario que contiene los campos para agregar un horario.
    
    :cvar hora_inicio: Hora de inicio del horario.
    :cvar hora_fin: Hora de finalizacion del horario.
    :cvar precio: Precio del horario.
    """
    hora_inicio = forms.IntegerField()
    hora_fin = forms.IntegerField()
    precio = forms.FloatField()


class EmpresaForm(ModelForm):
    """
    Formulario que renderiza los campos de la pagina de registro de una Empresa.

    :cvar departamento: ModelChoiceField que parsea el departamento
    :cvar provincia: ModelChoiceField que parcea la provincia
    """
    departamento = ModelChoiceField(
        queryset=Departamento.objects.all(), required=False)
    provincia = ModelChoiceField(
        queryset=Provincia.objects.all(), required=False)

    class Meta:
        model = Empresa
        fields = (
            'nombre', 'ruc', 'departamento', 'provincia', 'distrito', 'direccion',
            'tipo_servicio', 'telefono', 'representante', 'hora_apertura',
            'hora_cierre',
        )
