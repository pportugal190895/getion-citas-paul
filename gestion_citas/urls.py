"""gestion_citas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.views.static import serve

from rest_framework.authtoken import views

from customauth.views import (
    CustomLoginView,
    CustomLogoutView,
    RegistroUsuarioAdministradorFormView,
    RegistroUsuarioClienteFormView,
    HomeView,
    HomeEnterpriseView,
    HomeClientView,
    ListarProvinciasView,
    ListarDistritosView,
    ActualizarContrasenaUpdateView,
    UsuarioClienteUpdateView,
)
from gestion_citas import settings

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('home-enterprise', HomeEnterpriseView.as_view(),
         name='home-enterprise-view'),
    path('home-client', HomeClientView.as_view(), name='home-client-view'),
    path('admin/', admin.site.urls),
    path('site-login/', CustomLoginView.as_view(), name='site-login'),
    path('servicios/', include('servicios.urls')),
    path('reservas/', include('reservas.urls')),
    path('searches/', include('searches.urls')),
    path('enfermedad/', include('enfermedad.urls')),
    path('api-token-auth/', views.obtain_auth_token),
    path('site-logout/', CustomLogoutView.as_view(), name='site-logout'),
    path('config/', include('customauth.urls')),
    path('site-logout/', CustomLogoutView.as_view(), name='site-logout'),
    path('registro-nuevos-usuario-administrador/',
         RegistroUsuarioAdministradorFormView.as_view(),
         name='registro-nuevos-usuario-administrador'),
    path('registro-nuevos-usuario-cliente/',
         RegistroUsuarioClienteFormView.as_view(),
         name='registro-nuevos-usuario-cliente'),
    path('perfil/datos-personales/editar/<int:pk>/',
         UsuarioClienteUpdateView.as_view(),
         name='editar-client-datospersonales'),
    path('ajax/cargar_provincias/', ListarProvinciasView.as_view(),
         name='ajax_cargar_provincias'),
    path('ajax/cargar_distritos/', ListarDistritosView.as_view(),
         name='ajax_cargar_distritos'),
    path('editar/contrasena/<int:pk>',ActualizarContrasenaUpdateView.as_view(),
         name='editar-usuario-contrasena'),
]

urlpatterns += [
    path('api/rest/rc/', include('servicios.rest_urls')),
    path('api/rest/c/', include('customauth.rest_urls')),
    path('api/rest/r/', include('reservas.rest_urls')),
]

urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve, {
            'document_root': getattr(settings, "MEDIA_ROOT", None),
        }),
    ]
