# Sistema de Gestion Citas
Este sistema esta actualmente desarrollado para Sistema suministro servicios SRL

## Sistema de Administradores de Citas
Flujo de administradores

## Instalacion

* Crea un Entorno Virtual para la intalacion de los paquetes necesarios con el siguiente comando
```bash
    python -m venv virtual-env
```

* Clona el proyecto desde el siguiente [Enlace](https://gitlab.com/mmestas/gestion-citas)
```bash
    git@gitlab.com:mmestas/gestion-citas.git
```

* Si tu sistema operativo es Windows ubicate en el fichero **virtual-env\Scripts** y ejecuta el siguiente comando para activar el entorno virtual, en Caso de ser Linux la ruta es **virtual-env\bin\activate**
```bash
    activate.bat  (windows)
    source activate.bat  (Linux)
``` 

* Una vez activado el Entorno Virtual es necesario instalar los paquetes, para esto regresa a la ubicacion del proyecto 
y ejecuta el siguiente comando
```bash
    pip install -r requirements.txt
```
* Crea un archivo con extencion **.env** fuera del fichero del proyecto, en este podras configurar las variables unicas de la version de tu proyecto
```bash
    DEBUG=on
    SECRET_KEY=&x6h_qriq)sc29jsp(@ob%zkujaf=_!c517a4y&2#mmp-f*h
    ALLOWED_HOSTS=192.168.0.85,localhost
```

* La estructura de archivos deberia tener la siguiente forma 
```
    ├── virtual-env
    │   ├── Include
    │   ├── Lib
    │   ├── Scripts(Windows)/Source(Linux)
    │   ├── MarketPlaceProject
    │   │   ├── ...
    │   │   ├── marketplace_generic
    │   │   ├── manage.py
    │   │   ├── requirements.txt
    │   │   └── ...
    │   ├── .env
    │   └── ...
    └──
```

* Crea una Base de Datos y posteriormente configura la conección de tu base de datos en el archivo de variables de entorno, en este caso la coneccion es con PostgresSQL 
```bash
    DEBUG=on
    SECRET_KEY=&x6h_qriq)sc29jsp(@ob%zkujaf=_!c517a4y&2#mmp-f*h
    ALLOWED_HOSTS=192.168.0.85,localhost
    DATABASE_URL=psql://postgresusername:postgrespassword@localhost:5432/databasename
```

* Ahora es necesario Migrar los modelos de la siguiente manera
```bash
    python manage.py migrate
```

* Correr Comando para llenar las tablas de ubigeo: 
```bash
    python manage.py migrar_ubigeo 
```

* Correr comando para llenar las tablas de Tipos de Servicio
```
    python manage.py migrar_protocolos
```

* Si un modelo es modificado se tiene que actualizar las migraciones y volver a migrar
```bash
    python manage.py makemigrations
    python manage.py migrate
```

* Para finalizar iniciaremos el servidor y ejecutaremos el proyecto
```bash
    python manage.py runserver
```


## Sistema de Usuarios (Backend)
* Crear usuario de tipo 'f'(Usuario final)
* Hacer **post** al siguiente url: 
    ```
    http://localhost:8000/api-token-auth/
    
    data: 
    {
        "username": "username@correo.com",
        "password": "password"
    }
    ```
    El resultado debe ser un string con muchas letras, llamado token por ejemplo **47d71ef1833e23f31178d0430c778d3ef0d512cc**
*  Luego para hacer cualquier otra peticion debemos agregarle a los **Headers**
el siguiente parametro 
    ```
    {
        "Authorization": "Token 47d71ef1833e23f31178d0430c778d3ef0d512cc"
    }
    ```
    
## Guia de rest framework para front end
1. Login.
    - Enviar a: http://localhost:8000/api-token-auth/
    - Metodo: POST
    - Status: 200 (Ok)
    - Body:
    ```
    {
        "username": "usuario@correo.com",
        "password": "password*****"
    }
    ```
    - Return:
    ```
    {
        "Authorization": "Token 47d71ef1833e23f31178d0430c778d3ef0d512cc"
    }
    ```
    
2. Registro.
    - Enviar a: http://localhost:8000/api/rest/c/auth/register/
    - Metodo: POST
    - Status: 201 (Created)
    - Body: 
    ```
    {
        "email": "username@correo.com",
        "password": "password*****",
        "datospersonales": {
            "dni": "12345678",
            "nombres": "Jose",
            "apellidos": "Domingo Perez",
            "telefono": "987654321"
        }
    }
    ```
    - Return:
    ```{
        "email": "username@correo.com",
        "password": "pbkdf2_sha256$120000$d34ocJsgBOG1$2EeG8C8295tEY6PQ/GPMl9ZGD5u8oNWjmgCkb1g0eQ8=",
        "datospersonales": {
            "dni": "12345678",
            "nombres": "Jose",
            "apellidos": "Domingo Perez",
            "telefono": 987654321
        }
    }
    ```
    
3. Actualizar datos personales.
    - Enviar a: http://localhost:8000/api/rest/c/auth/emailusers/ID-EmailUser/
        - ID-EmailUser: id del usuario que se quiere actualizar.
    - Metodo: PUT 
    - Headers: Authorization: Token generado del login.
    - Status: 200 (Ok)
    - Body: 
    ```
    {
         "email": "username@gmail.com",
         "datospersonales": {
             "nombres": "Jose",
             "apellidos": "Maria Arguedas",
             "telefono": 987654322
         }
    }
    ```
    - Return: 
    ```
    {
         "email": "username@gmail.com",
         "datospersonales": {
             "nombres": "Jose",
             "apellidos": "Maria Perez",
             "telefono": 987654322
         }
    }
    ```
    
4. Obtener las reservas vigentes hasta el dia de hoy.
    - Enviar a: http://localhost:8000/api/rest/r/reservas/reservas/vigentes/
    - Metodo: GET
    - Headers: Authorization: Token generado del login.
    - Status: 200 (Ok)
    - Return: 
    ```
    [
        {
            "id": 49,
            "cancha_empresa_nombre": "EMPRESA1.srl",
            "inicio_hora_reserva": "15:00:00",
            "fin_hora_reserva": "17:00:00",
            "fecha": "2019-03-21"
        },
        {...},
    ]
    ```
   
5. Obtener las empresas favoritas del usuario.
    - Enviar a: http://localhost:8000/api/rest/rc/reserva-canchas/empresas/favoritas/
    - Metodo: GET
    - Headers: Authorization: Token generado del login.
    - Status: 200 (Ok)
    - Return: 
    ```
    [
        {
            "id": 1,
            "nombre": "EMPRESA1.srl",
            "direccion": "Calle Empresa nro 1"
        },
        {...}
    ]   
    ```
    
6. Agregar y borrar empresas favoritas.
    - Enviar a: http://localhost:8000/api/rest/rc/reserva-canchas/empresas/ID-EMPRESA/set_unset_favoritas/
        - ID-EMPRESA: ID de la empresa que se quiere agregar o borrar de los favoritos.
    - Metodo: GET
    - Headers: Authorization: Token generado del login.
    - Status: 201 (Created) - 200 (OK)
    - Return: 
    ```
    "Creado" 
    ```
    ```
    "Borrado"
    ```
    
7. Busqueda basica con fecha, hora inicio y hora fin.
    - Enviar a http://localhost:8000/api/rest/rc/reserva-canchas/empresas/busqueda/FECHA/HORA-INICIO/HORA-FIN/
        - FECHA: La fecha en formato "2019-12-31".
        - HORA-INICIO: Hora de inicio para el marguen de busqueda ejm(10).
        - HORA-FIN: Hora de fin para el marguen de busqueda ejm(14).
    - Metodo: GET
    - Headers: Authorization: Token generado del login.
    - Status: 200 (Ok)
    - Return: 
    ```
    [
        {
            "id": 1,
            "nombre": "EMPRESA LAS CANCHAS.SRL",
            "direccion": "calle LA EMPRESA NRO 1",
            "icono": "http://localhost:8000/media/media/configuraciones/logo.png"
        },
        {...},
    ]
    ```

8. Detalle de los datos de una empresa.
    - Enviar a: http://localhost:8000/api/rest/rc/reserva-canchas/empresa-cancha/detalle/ID-EMPRESA/
        - ID-EMPRESA: Id de la Empresa de canchas de la cual se quiere sus datos
    - Metodo: GET
    - Status: 200 (Ok)
    - Return:
    ```
    {
        "nombre": "EMPRESA LAS CANCHAS.SRL",
        "direccion": "calle LA EMPRESA NRO 1",
        "distrito": "Arequipa",
        "provincia": "Arequipa",
        "departamento": "Arequipa"
    }
    ```

9. Busqueda de canchas por empresa con fecha, hora inicio y hora fin.
    - Enviar a http://localhost:8000/api/rest/rc/reserva-canchas/empresas/ID_EMPRESA/busqueda/canchas/HORA_INICIO/HORA_FIN/FECHA/
        - ID-Empresa: Id de la empresa a la que pertenece la cancha. 
        - HORA-INICIO: Hora de inicio para el marguen de busqueda ejm(10).
        - HORA-FIN: Hora de fin para el marguen de busqueda ejm(14).
        - FECHA: La fecha para la busqueda en formato "2019-12-31".
    - Metodo: GET
    - Headers: Authorization: Token generado del login.
    - Status: 200 (Ok)
    - Return: 
    ```
    [
        {
            "id": 1,
            "identificador": "cancha-ELCOLOSO",
            "foto_cancha": "http://localhost:8000/media/media/m8l0lo1kpohy.png",
            "precio_total": 100
        },
        {...},
    ]
    ```

10. Detalle de los datos de una cancha con sus reseñas.
    - Enviar a: http://localhost:8000/api/rest/r/reservas/reviews_cancha/ID-Cancha/
        - ID-Cancha: Id de la cancha de la cual se quiere sus reviews
    - Metodo: GET 
    - Status: 200 (Ok)
    - Return:
    ```
    {
        "identificador": "cancha EL COLOSO",
        "tipo_cancha": "fu",
        "precio": 60,
        "largo": 40,
        "ancho": 20,
        "direccion_empresa": "empresa1",
        "reviews": [
            {
                "puntaje": 5,
                "descripcion": "DESCRIPCION"
            },
            {...},
        ]
    }
    ```
    
11. Validar la disponibilidad de una cancha.
    - Enviar a: http://localhost:8000/api/rest/r/reservas/reservas/validar/cancha/ID-Cancha/HORA-INICIO/HORA-fin/FECHA/
        - ID-Cancha: Id de la Cancha que se quiere validar. 
        - HORA-INICIO: Hora de inicio para el marguen de busqueda ejm(10).
        - HORA-FIN: Hora de fin para el marguen de busqueda ejm(14).
        - FECHA: La fecha para la busqueda en formato "2019-12-31".
    - Metodo: GET
    - Headers: Authorization: Token generado del login.
    - Status : 200 (Ok) - 400 (Bad Request)
    - Return :
    ```
    "La cancha esta disponible."
    ```
    ```
    "La cancha no esta disponible."
    ```
    
12. Confirmar reservacion de cancha.
    - Enviar a: http://localhost:8000/api/rest/r/reservas/reservas/confirmar_reserva/
    - Metodo: POST
    - Headers: Authorization: Token generado del login.
    - Body: 
    ```
    {
        "fecha": "2019-12-31",
        "inicio_hora_reserva": "14:00",
        "fin_hora_reserva": "16:00",
        "cancha": 1,
        "precio_hora": 15.00,
        "precio_total": 30.00,
        "descuento": 0,
        "estado": "1r"
    }
    ```
    - Status : 201 (Created) - 400 (Bad Request)
    - Return :
    ```
    "Cancha ceada con exito."
    ```
    ```
    "La cancha no esta disponible."
    ```
    
13. Detalle de una reserva.
    - Enviar a: http://localhost:8000/api/rest/r/reservas/reservas/ID_RESERVA/
        - ID_RESERVA: El id de a reserva de la cual se quiere saber sus datos.
    - Metodo: GET
    - Headers: Authorization: Token generado del login.
    - Status : 200 (Ok)
    - Return:
    ```
    {
        "id": 46,
        "fecha": "2019-03-20",
        "inicio_hora_reserva": "14:00:00",
        "fin_hora_reserva": "16:00:00",
        "estado": "1r",
        "cancha_empresa_nombre": "Empresa 1.SRL",
        "precio_total": 30,
        "cancha_empresa_telefono": "123456789"
    }
    ```
    