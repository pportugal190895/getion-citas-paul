import datetime

from django.contrib.auth import login, authenticate
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LogoutView
from django.core.files.images import get_image_dimensions
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views import View
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import FormView, CreateView, UpdateView

from rest_framework.exceptions import ValidationError

from reservas.models import Reserva
from servicios.models import Profesional, TipoServicio, Provincia, Distrito
from customauth.constants import DEFAULT_WALLPAPER
from customauth.models import (
    PersonalizacionPagina,
    EmailUser,
    EmpresaFavorita,
    DatosPersonales,
)
from customauth.forms import (
    RegistroUsuarioAdministradorForm,
    CustomAuthenticationForm,
    RegistroUsuarioClienteForm,
    RegistroPreguntaUsuarioForm,
    EditarContrasenaForm,
    DatosPersonalesForm,
)
from customauth.tokens import account_activation_token
from customauth.utils import send_email_activar_cuenta


class CustomLoginView(FormView):
    """
    Vista que hereda de una clase FormView, encargada de realizar el log-in
    del site.

    :cvar template_name: Nombre del archivo Html a renderizar.
    :cvar form_class: Formulario que se usara para la vista.
    """
    template_name = 'home_login.html'
    form_class = CustomAuthenticationForm

    def get_success_url(self):
        """
        Metodo usado para redireccionar una vez que el formulario sea validado.

        :return: metodo de redireccion por nombre de url.
        """
        if self.request.user.type_user == EmailUser.ADMIN:
            return reverse('home-enterprise-view')
        return reverse('home-client-view')

    def form_valid(self, form):
        """
        Metodo que se llama una vez que el formulario sea valido.

        :param form: formulario que contiene la imformacion del usuario.

        :return: HttpResponseRedirect que hace una llamada a la funcion
        get_success_url.
        """
        login(self.request, form.get_user())
        return HttpResponseRedirect(self.get_success_url())

    # Decorador encargado de verificar las letras sensibles como contraseñas.
    @method_decorator(sensitive_post_parameters())
    # Decorador encargado de brindar proteccion CSRF para la vista.
    @method_decorator(csrf_protect)
    # Decorador encargado de que los datos no se guarden en la memoria cache.
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        """
        Metodo encaragado de verificar si el usuraio es autenticado para
        redireccionar al home.

        :param request: Parametro que garda la informacion de la pagina.

        :return:Actualizacion del metodo dispatch().
        """
        if self.request.user.is_authenticated:
            redirect_to = self.get_success_url()
            if redirect_to == self.request.path:
                raise ValueError(
                    "Redirection loop for authenticated user detected. Check that "
                    "your LOGIN_REDIRECT_URL doesn't point to a login page."
                )
            return HttpResponseRedirect(redirect_to)
        return super().dispatch(request, *args, **kwargs)


class HomeEnterpriseView(LoginRequiredMixin, View):
    """
    Vista encargada de realizar la pagina principal home_enterpise.html
    """

    def get(self, request):
        """
        Metodo Get para renderizar la vista home_enterpise.html.

        :param request: Parametro que guarda la informacion de la pagina.

        :return: Metodo de renderizado de la pagina con datos correspondientes.
        """
        config = PersonalizacionPagina.objects.filter(
            empresa=request.user.empresa)
        profesionales = Profesional.objects.filter(
            empresa=request.user.empresa)[:3]
        reservas_dia = Reserva.objects.filter(
            fecha=datetime.date.today(),
            profesional__empresa=request.user.empresa
        ).count()
        context = {
            'config_imagen_pagina_principal':
                config.first().imagen_pagina_principal.url if (
                    config.exists() and config.first(
                ).imagen_pagina_principal != '') else
                DEFAULT_WALLPAPER,
            'profesionales': profesionales,
            'reservas_dia': reservas_dia
        }
        return render(request, 'home_enterpise.html', context)


class HomeClientView(View):
    """
    Vista que renderiza la pagina principal de un usuario Cliente
    """

    def get(self, request):
        """
        Metodo Get usado para renderizar la pagina y crear los datos nesesarios
        para esta como contexto.

        :param request:parametro que guarda la informacion de la pagina
        :return: Metodo de renderizado
        """
        reservas = Reserva.objects.filter(
            email_user=request.user, fecha__gte=datetime.date.today()
        ).order_by('fecha')
        reservas_vigentes = reservas.exclude(reviews__isnull=True)[:3]
        reservas_sin_resena = reservas.filter(reviews__isnull=True)[:3]
        empresas_favoritas = EmpresaFavorita.objects.filter(
            usuario=request.user)[:3]
        datos_personales = DatosPersonales.objects.filter(
            email_user=request.user).first()
        context = {
            'reservas_vigentes': reservas_vigentes,
            'reservas_sin_resena': reservas_sin_resena,
            'empresas_favoritas': empresas_favoritas,
            'datos_personales': datos_personales,
        }
        return render(request, 'home_client.html', context=context)


class HomeView(View):
    """
    Vista Generica encargada de renderizar la pagina principal.
    """

    def get(self, request):
        """
        Metodo Get usado para renderizar la pagina y sus datos.

        :param request: Parametro que guarda la informacion de la pagina.

        :return: Metodo de renderizacion.
        """
        tipos_servicio = TipoServicio.objects.all()[:3]

        contexto = {
            'tipos_servicio': tipos_servicio,
        }
        return render(request, 'home.html', context=contexto)


class CustomLogoutView(LogoutView):
    """
    Vista encargada del cierre de sesion del usuario.

    :cvar next_page: metodo de redireccion por nombre de url.
    """
    next_page = reverse_lazy('home')


class PersonalizacionPaginaCreateView(CreateView):
    """
    Vista que crea la configuracion para una empresa
    """
    model = PersonalizacionPagina
    template_name = 'personalizacionpagina_create.html'
    fields = ['imagen_pagina_principal', 'color_paginas_secundarias',
              'icono_logo_empresa', ]
    non_shown_errors = None

    def get_success_url(self):
        return reverse('home-enterprise-view')

    def form_valid(self, form):
        """
        Funcion agrega la empresa y en caso se agregue el icono valida el taman
        """
        form.instance.empresa = self.request.user.empresa
        icono_logo_empresa = form.cleaned_data['icono_logo_empresa']
        if icono_logo_empresa:
            width, height = get_image_dimensions(icono_logo_empresa)
            if width > 100 or height > 100:
                self.non_shown_errors = (
                    "La imagen no debe tener mas de 65 px de altura o ancho")
                return self.form_invalid(form)
        return super(PersonalizacionPaginaCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        """
        Funcion agrega los campos que no son mostrados por la funcion
        form.add_error

        :return: Dict del formulario
        """
        context = super(PersonalizacionPaginaCreateView,
                        self).get_context_data()
        context['non_shown_errors'] = self.non_shown_errors
        return context


class PersonalizacionPaginaUpdateView(UpdateView):
    """
    Personalizacion que actualiza la pagina de personalizacion
    """
    model = PersonalizacionPagina
    template_name = 'personalizacionpagina_update.html'
    fields = ['imagen_pagina_principal', 'color_paginas_secundarias',
              'icono_logo_empresa']
    non_shown_errors = None

    def form_valid(self, form):
        """
        Funcion valida 'icono_logo_empresa' y el tamaño del mismo campo, en caso
        no tenga las medidas maximas, mostrara un mensaje
        """
        icono_logo_empresa = form.cleaned_data['icono_logo_empresa']
        if icono_logo_empresa:
            width, height = get_image_dimensions(icono_logo_empresa)
            if width > 100 or height > 100:
                error_message = (
                    'La imagen no debe tener mas de 65 px de altura o ancho')
                form.add_error(
                    'icono_logo_empresa', ValidationError(error_message))
                self.non_shown_errors = error_message
                return self.form_invalid(form)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('home-enterprise-view')

    def get_context_data(self, **kwargs):
        """
        Funcion agrega los campos que no son mostrados por la funcion
        form.add_error

        :return: Dict del formulario
        """
        context = super(PersonalizacionPaginaUpdateView,
                        self).get_context_data()
        context['non_shown_errors'] = self.non_shown_errors
        return context


class PersonalizacionPaginaCreateUpdateRedirectionView(View):
    """
    Clase encargada de redireccionar a crear o actualizar
    """

    def get(self, request):
        """
        Funcion que redirecciona al usuario a que cree su configuracion deseada
        o actualice la anteriormente creada

        :return: Redirect
        """
        personalizacion = PersonalizacionPagina.objects.filter(
            empresa=request.user.empresa)
        if personalizacion.exists():
            return HttpResponseRedirect(
                reverse('personalizationpage-update',
                        kwargs={'pk': personalizacion.first().id})
            )
        return HttpResponseRedirect(reverse('personalizationpage-create'))


class RegistroUsuarioAdministradorFormView(FormView):
    """
    Vista que renderiza la pagina de registro de un usuario tipo Administrador.
    link: http://localhost:8000/registro-nuevos-usuario-administrador/

    :cvar form_class: Variable, guarda el formulario a ser usado.
    :cvar template_name: Variable, guarda el nombre del template a renderizar.
    """
    form_class = RegistroUsuarioAdministradorForm
    template_name = 'register_admin_user.html'

    def get_success_url(self):
        """
        Metodo que redirecciona una vez validado el formulario.

        :return: Metodo Reverse de redireccion.
        """
        url_name = 'site-login'
        return reverse(url_name)

    def form_valid(self, form):
        """
        Metodo que valida el formulario, y envia un email al usuario para que
        pueda activar su cuenta, de otra manera no podra ingresar al sistema

        :param form: formulario a usarse.

        :return: Metodo de validacion del formulario.
        """
        user = form.save()
        send_email_activar_cuenta(user)
        return super(RegistroUsuarioAdministradorFormView, self).form_valid(form)


class RegistroUsuarioClienteFormView(FormView):
    """
    Vista que hereda de FormView usado para renderizar la pagina de registro de
    un usuario final.
    link: http://localhost:8000/registro-nuevos-usuario-cliente/

    :cvar form_class: Variable, guarda el formulario a ser usado.
    :cvar template_name: Variable, guarda el nombre del template a renderizar.
    """
    form_class = RegistroUsuarioClienteForm
    template_name = 'register_client_user.html'

    def get_success_url(self):
        """
        Metodo usado para redireccionar a la pagina de login cuando el
        formulario sea validado.

        :return: Metodo de redireccionamiento.
        """
        url_name = 'site-login'
        return reverse(url_name)

    def form_valid(self, form):
        """
        Metodo que valida el formulario, y envia un email al usuario para que
        pueda activar su cuenta, de otra manera no podra ingresar al sistema

        :param form: formulario a usarse.

        :return: Metodo de validacion del formulario.
        """
        user = form.save()
        send_email_activar_cuenta(user)
        kwargs = {'id_user': user.pk}
        return HttpResponseRedirect(
            reverse('preactivacion-email', kwargs=kwargs))


class RegistroPreActivacion(View):
    """
    Class usada para renderizar
    """

    def get(self, request, id_user):
        """
        Metodo usado para renderizar la pagina de preactivacion de usuario

        :param request: Parametro que guarda los datos de la pagina.
        :param id_user: Id de usuario registrado

        :return: Metodo de renderizado.
        """
        user = EmailUser.objects.get(id=id_user)
        context = {'user': user}
        return render(request, 'register_preactivate.html', context=context)

    def post(self, request, id_user):
        """
        Metodo que reenvia el correo de activacion.

        :return: Metodo de redireccionamiento hacia que actualiza la pagina.
        """
        user = EmailUser.objects.get(id=id_user)
        send_email_activar_cuenta(user)
        kwargs = {'id_user': user.pk}
        return HttpResponseRedirect(
            reverse('preactivacion-email', kwargs=kwargs))


class UsuarioClienteUpdateView(UpdateView):
    """
    Vista que hereda de FormView usado para renderizar la pagina de registro de
    datos personales.
    link: http://localhost:8000/editar/datos/cliente/pk/
    """
    model = DatosPersonales
    form_class = DatosPersonalesForm
    template_name = 'client_datospersonales.html'

    def get_object(self):
        """
        Metodo get_object asigna un objeto especifico

        :return: Retorna el objeto filtrado
        """
        return DatosPersonales.objects.get(email_user_id=self.request.user.id)

    def get_success_url(self):
        """
        Metodo get_success_url que redireciona a la vista home de empresas

        :return: Metodo reverse usado para redireccionar
        """
        return reverse(
            'editar-client-datospersonales',
            kwargs={'pk': self.request.user.id}
        )


class ActivarEmailUserView(View):
    """
    Vista para activar a los usuarios una vez ingresen al enlace del email
    """

    def get(self, request, uidb64, token):
        """
        Funcion recibe el id codificado en base64 y el token para poder validar
        el email

        :param uidb64: Base64 String conteniendo el id del mail
        :param token: String con un token de verificacion

        :return: Redirecciona a al home
        """

        # Intentamos decodificar el string para convertirlo en un numero entero,
        # que seria el id
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = EmailUser.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        # Validamos que el usuario exista y que el usuario y el token
        # conrrespontan
        if user is not None and account_activation_token.check_token(
                user, token):
            user.is_active = True
            user.save()
            return redirect(
                'activation_email_view')
        else:
            return HttpResponse('Activation link is invalid!')


class EstadisticasView(View):
    """
    Vista que implementara las estadisticas
    """

    # Todo: Implementar, cuando sepamos que estadisticas sacar.
    def get(self, request):
        return render(request, 'estadisticas.html')


class ReestablecerConfiguracionView(View):
    """
    Vista que que reestablece el color de la barra de navegacion de la pagina.
    """

    def post(self, request):
        """
        Metodo que actualiza el color de la pagina del usuario.

        :param request: Parametro que guarda la informacion de la pagina.

        :return: redirect hacia la pagina principal.
        """
        configuracion = PersonalizacionPagina.objects.filter(
            empresa=request.user.empresa).first()
        configuracion.color_paginas_secundarias = '#343a40'
        configuracion.save()
        return redirect('home-enterprise-view')


class HomePreguntaUsuarioFormView(FormView):
    """
    Vista que muestra el formulario preguntas para el usuario.
    link: http://localhost:8000/home_question_user/
    """
    form_class = RegistroPreguntaUsuarioForm
    template_name = 'home_question_user.html'

    def get_success_url(self):
        """
        Funcion que redirecciona una vez haya sido guardado el objeto con exito

        :return: url por nombre que redirecciona a la vista de home
        """
        url_name = 'home'
        return reverse(url_name)

    def form_valid(self, form):
        """
        Funcion que se llama una vez el formulario sea valido

        :param form: Obj que contiene la informacion a guardar

        :return: HttpResponseRedirect que llama a la funcion get_success_url
        """
        object = form.save()
        object.empresa = self.request.user.empresa
        object.save()
        return HttpResponseRedirect(self.get_success_url())


class ListarProvinciasView(View):
    """
    Vista que muestra la lista de las provincias de un departamento selecionado
    """
    template_name = 'listaCiudades/provincias_dropdown_list.html'

    def get(self, request):
        """
        Funcion que recibe el id del departamento selecionado y filtra las provincias
        pertenecientes a este.

        :return: Redirecciona a la lista de provincias
        """
        departamento_id = request.GET.get('departamento')
        provincias = Provincia.objects.filter(
            departamento_id=departamento_id).order_by('nombre')

        return render(request, self.template_name, {'provincias': provincias})


class ListarDistritosView(View):
    """
    Vista que muestra la lista de los distritos de una provincia selecionada.
    """
    template_name = 'listaCiudades/distritos_dropdown_list.html'

    def get(self, request):
        """
        Funcion que recibe el id de la provincia selecionado y filtra los distritos
        pertenecientes a este.

        :return: Redirecciona a la lista de distritos
        """
        provincia_id = request.GET.get('provincia')
        distritos = Distrito.objects.filter(
            provincia_id=provincia_id).order_by('nombre')

        return render(request, self.template_name, {'distritos': distritos})


class ActivacionEmailView(View):
    """
    Vista Generica encargada de Indicar activacion satisfactoria de correo.
    """

    def get(self, request):
        """
        Funcion que se encarga de renderizar el get.
        """
        return render(request, 'home_activation_email_update.html')


class ActualizarContrasenaUpdateView(UpdateView):
    """
    Vista que renderiza la pagina de registro de un usuario.
    link: http://localhost:8000/editar/usuario/pk

    :cvar form_class: Variable, guarda el formulario a ser usado.
    :cvar template_name: Variable, guarda el nombre del template a renderizar.
    """
    model = EmailUser
    form_class = EditarContrasenaForm
    template_name = 'edit_user.html'

    def get_success_url(self):
        """
        Metodo get_success_url que redireciona a la vista login despues de cambiar
        la contraseña

        return: Metodo reverse usado para redireccionar
        """
        return reverse('site-login')
