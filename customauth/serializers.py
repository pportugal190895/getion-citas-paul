from django.contrib.auth.password_validation import validate_password

from drf_writable_nested import WritableNestedModelSerializer
from rest_framework import serializers

from customauth.utils import send_email_activar_cuenta
from customauth.models import EmailUser
from customauth.models import DatosPersonales


class DatosPersonalesSerializer(serializers.ModelSerializer):
    """
    Serializer para datos personales del usuario con los campos dni, nombres,
    apellidos y telefono.
    {
        "dni": 88888888,
        "nombres": "juanito",
        "apellidos": "zuares",
        "telefono": 987654321
    }
    """
    class Meta:
        model = DatosPersonales
        fields = ('dni', 'nombres', 'apellidos', 'telefono')


class EmailUserSerializer(WritableNestedModelSerializer):
    """
    Serializer para el EmailUser con el email, password y los datos personales
    {
        "email": "usuario@correo.com",
        "password": ***********,
        "datospersonales": {
            "dni": 88888888,
            "nombres": "juanito",
            "apellidos": "zuares",
            "telefono": 987654321
    }
    """
    datospersonales = DatosPersonalesSerializer(many=False)
    password = serializers.CharField()

    class Meta:
        model = EmailUser
        fields = ('email', 'password', 'datospersonales')

    def validate_password(self, value):
        """
        Funcion valida la contraseña

        :param value: Str que contiene la contraseña
        :return: Str validado
        """
        validate_password(value)
        return value

    def create(self, validated_data):
        """
        Crea al usuario junto con sus datos personales y le agrega la contraseña
        al usuario

        :param validated_data: QueryDict que contiene los datos del request.
        :return: Instancia
        """
        password = validated_data.pop('password')
        instance = super(EmailUserSerializer, self).create(validated_data)
        instance.set_password(password)
        instance.save()
        send_email_activar_cuenta(instance)
        return instance


class DatosPersonalesUpdateSerializer(serializers.ModelSerializer):
    """
    Serializer para datos personales del usuario con los campos, nombres,
    apellidos y telefono.
    {
        "nombres": "juanito",
        "apellidos": "zuares",
        "telefono": 987654321
    }
    """
    class Meta:
        model = DatosPersonales
        fields = ('nombres', 'apellidos', 'telefono')


class EmailUserUpdateSerializer(serializers.ModelSerializer):
    """
    Serializer para el EmailUser con el email y los datos personales sin password
    {
        "email": "usuario@correo.com",
        "datospersonales": {
            "nombres": "juanito",
            "apellidos": "zuares",
            "telefono": 987654321
    }
    """
    datospersonales = DatosPersonalesUpdateSerializer(many=False)

    class Meta:
        model = EmailUser
        fields = ('email', 'datospersonales')

    def update(self, instance, validated_data):
        """
        Metodo update implementado por ser serializadores anidados.

        :param instance: instancia EmailUser.
        :param validated_data: QueryDict que contiene los datos del request.
        :return: EmailUser actualizado.
        """
        datos_personales_data = validated_data.pop('datospersonales')
        datos_personales = instance.datospersonales
        for field, value in datos_personales_data.items():
            setattr(datos_personales, field, value)
        datos_personales.save()
        return instance



