from django import forms
from django.contrib import admin
from django.contrib.admin import ModelAdmin, TabularInline
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group

from customauth.models import (
    EmailUser,
    PersonalizacionPagina,
    DatosPersonales,
    EmpresaFavorita,
    PreguntaUsuario,
    MedioPago,
)
from servicios.models import Departamento, Provincia, Distrito
from rest_framework.authtoken.models import Token

from enfermedad.models import Cuarentena

class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = EmailUser
        fields = ('email', 'type_user')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.type_user = 'a'
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = EmailUser
        fields = (
            'email',
            'password',
            'type_user',
            'is_active',
            'is_admin',
            'empresa'
        )

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm
    # login_form = CustomAuthenticationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = (
        'email', 'is_admin', 'is_active', 'type_user', 'datospersonales')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Permissions', {'fields': ('type_user', 'is_admin', 'is_active')}),
        ('Empresa', {'fields': ('empresa',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


class DistritoAdmin(ModelAdmin):
    list_display = (
        'nombre', 'provincia', 'nombre_provincia', 'nombre_departamento'
    )
    search_fields = ('provincia__nombre', 'provincia__departamento__nombre')


class DistritoInline(TabularInline):
    model = Distrito
    extra = 1


class ProvinciaAdmin(ModelAdmin):
    list_display = ('nombre', 'departamento')
    search_fields = ('departamento__nombre', )
    inlines = [DistritoInline,]


class ProvinciaInline(TabularInline):
    model = Provincia
    extra = 1


class DepartamentoAdmin(ModelAdmin):
    inlines = [ProvinciaInline]


admin.site.register(EmailUser, UserAdmin)
admin.site.unregister(Group)
admin.site.register(PersonalizacionPagina)
admin.site.register(Departamento, DepartamentoAdmin)
admin.site.register(Provincia, ProvinciaAdmin)
admin.site.register(Distrito, DistritoAdmin)
admin.site.register(DatosPersonales)
admin.site.register(EmpresaFavorita)
admin.site.register(MedioPago)
admin.site.register(PreguntaUsuario)
admin.site.register(Cuarentena)
