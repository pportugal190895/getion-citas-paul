from django.urls import path

from customauth.views import (
    PersonalizacionPaginaCreateView,
    PersonalizacionPaginaUpdateView,
    PersonalizacionPaginaCreateUpdateRedirectionView,
    ActivarEmailUserView,
    EstadisticasView,
    ActivacionEmailView,
    ReestablecerConfiguracionView,
    HomePreguntaUsuarioFormView,
    RegistroPreActivacion,
)


urlpatterns = [
    path('personalizacion/', PersonalizacionPaginaCreateView.as_view(),
         name='personalizationpage-create'),
    path('personalizacion/<int:pk>/', PersonalizacionPaginaUpdateView.as_view(),
         name='personalizationpage-update'),
    path('personalizacion/redir/',
         PersonalizacionPaginaCreateUpdateRedirectionView.as_view(),
         name='personalizationpage'),
    path('personalizacion/cuentas/activar/<slug:uidb64>/<slug:token>/',
         ActivarEmailUserView.as_view(), name='activate'),
    path('estadisticas/', EstadisticasView.as_view(), name='estadisticas-list'),
    path('reestablecer/configuracion/color',
         ReestablecerConfiguracionView.as_view(),
         name='color-configuracion-update'),
    path('home_question_user/', HomePreguntaUsuarioFormView.as_view(),
         name='home_question_user'),
    path('reestablecer/configuracion/color',
         ReestablecerConfiguracionView.as_view(),
         name='color-configuracion-update'),
    path('activation_email/', ActivacionEmailView.as_view(),
         name='activation_email_view'),
    path('preactivacion_email/<int:id_user>/', RegistroPreActivacion.as_view(),
         name='preactivacion-email')
]

