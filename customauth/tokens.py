from django.contrib.auth.tokens import PasswordResetTokenGenerator
import six


class TokenGenerator(PasswordResetTokenGenerator):
    """
    Clase que se encarga de generar un token para asignar validar nuevo usuario
    """
    def _make_hash_value(self, user, timestamp):
        """
        Generates the user token based on the pk, time asked and whether the
        user is active or not
        """
        return (
            six.text_type(user.pk) + six.text_type(timestamp) +
            six.text_type(user.is_active)
        )


account_activation_token = TokenGenerator()

