from os.path import basename

from django.conf.urls import url
from django.urls import include

from rest_framework.routers import DefaultRouter

from customauth.rest_views import EmailUserCreateView, EmailUserUpdateView
from reservas.rest_views import ReservaViewSet


router = DefaultRouter()
router.register(r'ficha',  ReservaViewSet)
router.register(
    r'register', EmailUserCreateView, basename='emailuser_register'
)
router.register(
    r'emailusers', EmailUserUpdateView, basename='emailuser_update'
)

urlpatterns = [
    url(r'auth/', include(router.urls))
]
