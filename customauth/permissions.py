from rest_framework.permissions import BasePermission


class EndUserIsAuthenticated(BasePermission):
    """
    Filtro para los usuarios autorizados
    """

    def has_permission(self, request, view):
        """
        Funcion verifica que el usuario no sea anonimo, este autentificado y
        sea usuario final o administrador (estrictamente para administradores)

        :param request: Metadata que contiene al usuario
        """
        return request.user and request.user.is_authenticated and (
                request.user.type_user == 'f' or request.user.is_admin)
