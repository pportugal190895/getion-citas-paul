from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.db import models

from colorful.fields import RGBColorField
from servicios.models import TipoServicio


class EmailUserManager(BaseUserManager):
    """
    Clase que hereda del modelo BaseUserManager usado para entrar.
    """
    def create_user(self, email, password=None):
        """
        Metodo usado para crear un usuario con su correo y password.

        :param email: Str que contiene el email del usuario.
        :param password: Str que contiene la contraseña del usuario.

        :return: Obj del tipo User.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Metodo usado para crear un super usuario con su correo y password.

        :param email: Str que contiene el email del super usuario.
        :param password: Str que contiene la contraseña del super usuario.

        :return: Obj del tipo User.
        """
        user = self.create_user(
            email,
            password=password,
        )
        user.is_admin = True
        user.is_active = True
        user.type_user = 'a'
        user.save(using=self._db)
        return user


class EmailUser(AbstractBaseUser):
    """
    Modelado de el objeto EmailUser que hereda de un modelo abstracto
    AbstractBaseUser.

    :cvar email: EmailField que contiene el email de el Usuario.
    :cvar is_active: BooleanField que define si el usuario esta Activo o
        eliminado.
    :cvar is_admin: BooleanField que define si el usuario es Administrador.
    :cvar type_user: CharField que define el tipo del Usuario.
    :cvar empresa: ForeignKey que contiene la empresa en la que trabaja el
        usuario.
    """
    ADMIN = 'a'
    CLIENT = 'f'
    TYPE_USER = ((ADMIN, 'Administrador Empresa'), (CLIENT, 'Usuario final'),)
    email = models.EmailField(
        verbose_name='correo electronico',
        max_length=255,
        unique=True,
    )
    is_active = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    type_user = models.CharField(max_length=1, default='f', choices=TYPE_USER)
    objects = EmailUserManager()
    password = models.CharField(verbose_name='contraseña', max_length=128)

    USERNAME_FIELD = 'email'
    empresa = models.ForeignKey(
        'servicios.Empresa', null=True, blank=True,
        on_delete=models.CASCADE
    )

    def __str__(self):
        """
        Funcion que retorna el objeto al ser llamado, este retorna el correo del
            usuario.
        :return: Email del usuario.
        """
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        """
        Metodo usado para saber si es admin.

        :return: Boolean
        """
        return self.is_admin


class PersonalizacionPagina(models.Model):
    """
    Modelo de la pagina de personalizacion de la aplicacion

    :cvar empresa: OneToOneField relacionado a la empresa a la que pertenece
        esta clase
    :cvar imagen_pagina_principal: ImageField que guarda el fondo de pantatalla
    :cvar color_paginas_secundarias: RGBColorField que guarda un color para las
        paginas secundarias
    :cvar icono_logo_empresa: ImageField que guarda el logo de la empresa
    """
    empresa = models.OneToOneField(
        'servicios.Empresa', on_delete=models.CASCADE, unique=True)
    imagen_pagina_principal = models.ImageField(
        upload_to='media/configuraciones/', blank=True, null=True)
    color_paginas_secundarias = RGBColorField(
        max_length=9, null=True, blank=True)
    icono_logo_empresa = models.ImageField(
        upload_to='media/configuraciones/', blank=True, null=True)

    def __str__(self):
        return self.empresa.nombre


class DatosPersonales(models.Model):
    """
    Modelo que almacena los datos personales de un usuario.

    :cvar email_user: ForeingKey que guarda el email de un usuario.
    :cvar dni: Charfield que guarda el dni del usuario.
    :cvar nombres: Charfield que guarda el dni del usuario.
    :cvar apellido_paterno: Charfield que guarda apellido paterno del usuario.
    :cvar apellido_materno: Charfield que guarda apellido materno del usuario.
    :cvar telefono: Integerfield que guarda el telefono de un usuario.
    """
    email_user = models.OneToOneField(
        'EmailUser', on_delete=models.CASCADE)
    dni = models.CharField(unique=True, max_length=8)
    nombres = models.CharField(max_length=256)
    apellido_paterno = models.CharField(max_length=64)
    apellido_materno = models.CharField(max_length=64)
    telefono = models.IntegerField()

    def __str__(self):
        return "{}-{} {} {}".format(
            self.dni, self.nombres, self.apellido_paterno, self.apellido_materno
        )


class EmpresaFavorita(models.Model):
    """
    Modelo que almacenara las Empresas favoritas de los usuarios.

    :cvar usuario: Foreingkey que apunta a un usuario.
    :cvar empresa: Foreing key que apunta a una Empresa de profesional.
    """
    usuario = models.ForeignKey(EmailUser, on_delete=models.CASCADE)
    empresa = models.ForeignKey('servicios.Empresa', on_delete=models.CASCADE)

    class Meta:
        unique_together = ('usuario', 'empresa')

    def __str__(self):
        return f"{self.usuario.mail} - {self.empresa.nombre}"


class MedioPago(models.Model):
    """
    Modelo que almacena el medio de pago efectuado por un usuario.

    :cvar comprobante_pago: ImageField que guarda la imagen del comprobante.
    :cvar numero_operacion: Charfield que guarda el numero de operacion del
        comprobante.
    :cvar fecha: DateField que guarda la fecha de la opracion.
    :cvar monto: Charfield que guarda el mondo del pago.
    """

    comprobante_pago = models.ImageField(upload_to='media/')
    numero_operacion = models.CharField(max_length=256)
    fecha = models.DateField()
    monto = models.FloatField()

    def __str__(self):
        return f"{self.numero_operacion} - {self.fecha}"

        return f"{self.usuario.email} - {self.empresa.nombre}"


class PreguntaUsuario(models.Model):
    """
    Modelo que almacena los preguntas del usuario.

    :cvar email: EmailField que guarda el email de un usuario a consultar.
    :cvar mensaje: CharField que guarda el mensaje del usuario a consultar.
    :cvar tipo_servicio: ForeignKey que guarda el t.s. del usuario a consultar.
    """
    email = models.EmailField()
    mensaje = models.CharField(max_length=256)
    tipo_servicio = models.ForeignKey(TipoServicio, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.email} - {self.tipo_servicio}"

