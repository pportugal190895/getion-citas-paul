from django.http import HttpResponseRedirect
from django.urls import reverse

from customauth.models import PersonalizacionPagina
from servicios.models import TipoServicio

excluded_paths = ['site-login', 'admin', 'adminlogin', 'api-token-auth', 'registro-nuevos-usuarios']


def company_request_processor(request):
    """
    Funcion para el procesamiento global del contexto para el icono por empresa

    :param request: Objeto que contiene la Metadata de la peticion

    :return: Dict con el logo de la empresa
    """
    if request.user.is_authenticated:
        config = PersonalizacionPagina.objects.filter(
            empresa=request.user.empresa)
        if config.exists() and config.first().icono_logo_empresa != '':
            image = request.user.empresa.personalizacionpagina.icono_logo_empresa
            color = request.user.empresa.personalizacionpagina.color_paginas_secundarias
            return {'company_logo': image.url, 'email_principal': request.user, 'color_nav_bar': color}
    tipos_servicio = TipoServicio.objects.all()
    return {
        'company_logo': None,
        'email_principal': request.user,
        'color_nav_bar': '#343a40',
        'tipos_servicio': tipos_servicio
    }


class AuthRequiredMiddleware(object):
    """
    Clase Middleware implementa la logica de permisos del usuario registrado.
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def is_allowed(self, request):
        """
        Funcion que permite o no ingresar al usuario al url requerido.

        :param request: Request object que contiene la metadata del request.

        :return: Bool
        """
        # if encargado de verificar si se esta intentando acceder a un url de Rest
        # Framework
        """
        if "/api/rest/" in request.path:
            return False
        else:
            # if encargado de verificar que el usuario este autenticado y si no
            # que intente acceder a un url de path excluidos (de login)
            if not request.user.is_authenticated and request.path.replace(
                "/", "") not in excluded_paths:
                return True
                """
        return False

    def __call__(self, request):
        """
        Metodo usado para verificar el permiso del usuario.

        :param request: Parametro que guarda la informacion de las paginas.
        """
        response = self.get_response(request)

        # if encargado de verificar si ellos datos del request son validos,
        # retorna una redireccion hacia la pagina de registro login.
        if self.is_allowed(request):
            return HttpResponseRedirect(reverse('site-login'))
        # si no por defecto continuara con los mismos datos del response.
        return response