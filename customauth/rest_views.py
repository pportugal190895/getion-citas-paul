from customauth.models import EmailUser
from rest_framework import mixins, viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from customauth.serializers import (
    EmailUserSerializer,
    EmailUserUpdateSerializer
)


class EmailUserCreateView(GenericViewSet, mixins.CreateModelMixin):
    """
    ViewSet encargado de la creacion del Usuario
    Hacer POST a esta ruta:
    http://localhost:8000/api/rest/c/auth/register/
    """
    serializer_class = EmailUserSerializer


class EmailUserUpdateView(GenericViewSet, mixins.UpdateModelMixin):
    """
    ViewSet encargado de la actualizacion del Usuario con la
    Hacer PUT a esta ruta:
    http://localhost:8000/api/rest/c/auth/emailusers/ID-EmailUser/
    """
    serializer_class = EmailUserUpdateSerializer
    permission_classes = (IsAuthenticated, )
    queryset = EmailUser

    def update(self, request, *args, **kwargs):
        """
        Metodo encargado de verificar el usuario autenticado dueño de la
        informacion.

        :return:Response con los informacion de peticion, si es denegada o no.
        """
        email_user = self.get_object()
        if request.user.id == email_user.id:
            return super(EmailUserUpdateView, self).update(request, *args, **kwargs)
        else:
            data = {'no tiene permisos, para ingresar aqui'}
            return Response(data, status=status.HTTP_403_FORBIDDEN)

