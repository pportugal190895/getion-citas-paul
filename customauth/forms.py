import datetime

from django.contrib.auth.forms import AuthenticationForm
from django.forms import (
    forms,
    EmailInput,
    CharField,
    IntegerField,
    ModelForm,
    PasswordInput,
    TextInput,
    ModelChoiceField,
    NumberInput,
    Textarea,
)

from customauth.models import EmailUser, PreguntaUsuario, DatosPersonales
from servicios.models import (
    Empresa,
    TipoServicio,
    Departamento,
    Provincia,
    Distrito,
)


class CustomAuthenticationForm(AuthenticationForm):
    """
    Formulario que hereda de la clase AuthenticationForm, encargado de
    autenticar si el usuario que esta ingresando es del tipo Administrador
    de Empresas.

    :cvar username: EmailInput entrada del tipo email en el formulario.
    :cvar password: CharField entrada para la contrasenagit st
    """
    username = EmailInput()
    password = CharField(label='Contraseña', widget=PasswordInput)


class RegistroUsuarioAdministradorForm(ModelForm):
    """
    Formulario que renderiza los campos de la pagina de registro de un
    usuario tipo Adminitrador de Negocio.

    :cvar dni: Charfield que guarda el dni del usuario.
    :cvar nombres: Charfield que guarda los nombres del usuario.
    :cvar apellido_paterno: Charfield que guarda el apellido paterno del usuario.
    :cvar apellido_materno: Charfield que guarda el apellido materno del usuario.
    :cvar telefono: IntegerField que guarda el telefono del usuario.
    :cvar password1: Charfield que guarda el password del usuario.
    :cvar password2: Charfield que confirma el password del usuario.
    :cvar nombre_negocio: CharField que guarda el nombre del negocio.
    :cvar ruc: CharField que guarda el ruc de la empresa.
    :cvar departamento: ModelChoiceField que parsea el departamento
    :cvar provincia: ModelChoiceField que parcea la provincia
    :cvar distrito: ModelChoiceField que parcea el distrito
    :cvar direccion: CharField que guarda la direccion de la empresa.
    :cvar telefono_negocio: CharField que guarda el telefono de la empresa.
    :cvar representante: CharField que guarda el nombre del representante
        de la empresa.
    :cvar hora_apertura: IntegerField que guarda la hora de apertura de la
        empresa.
    :cvar hora_cierre: IntegerField que guarda la hora de cierre de la empresa.
    """
    dni = CharField(max_length=8, widget=TextInput(
        attrs={'type': 'number'}), label="DNI")
    nombres = CharField(max_length=256)
    apellido_paterno = CharField(max_length=64)
    apellido_materno = CharField(max_length=64)
    telefono = IntegerField()
    password1 = CharField(label='Password', widget=PasswordInput)
    password2 = CharField(
        label='Confirmar Password', widget=PasswordInput)
    nombre_negocio = CharField(max_length=129, required=True)
    ruc = CharField(max_length=11, required=True, label="RUC")
    departamento = ModelChoiceField(
        queryset=Departamento.objects.all(), required=True)
    provincia = ModelChoiceField(
        queryset=Provincia.objects.all(), required=True)
    distrito = ModelChoiceField(
        queryset=Distrito.objects.all(), required=True)
    direccion = CharField(max_length=200, required=True)
    tipo_servicio = ModelChoiceField(
        queryset=TipoServicio.objects.all(), required=True)
    telefono_negocio = CharField(max_length=9, required=True)
    representante = CharField(max_length=128, required=True)
    hora_apertura = IntegerField(
        max_value=23, min_value=0, required=True,
        widget=NumberInput(attrs={'placeholder': 'Ej: 8'}))
    hora_cierre = IntegerField(
        max_value=23, min_value=0, required=True,
        widget=NumberInput(attrs={'placeholder': 'Ej: 20'}))

    class Meta:
        model = EmailUser
        fields = (
            'email', 'password1', 'password2', 'dni', 'nombres',
            'apellido_paterno', 'apellido_materno', 'telefono',
            'nombre_negocio', 'ruc', 'departamento', 'provincia', 'distrito',
            'direccion', 'tipo_servicio', 'telefono_negocio', 'representante',
            'hora_apertura', 'hora_cierre',
        )

    def clean_password2(self):
        """
        Metodo que valida si las contraseñas coinciden.
        """
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def clean_dni(self):
        """
        Metodo que valida el campo de DNI el usuario.

        :return: dni.
        """
        dni = self.cleaned_data.get("dni")
        usuario = DatosPersonales.objects.filter(dni=dni).first()
        if usuario:
            raise forms.ValidationError("Existe un usuario con este dni.")
        return dni

    def clean_ruc(self):
        """
        Metodo que valida el campo RUC.

        :return: ruc
        """
        ruc = self.cleaned_data.get('ruc')
        empresa = Empresa.objects.filter(ruc=ruc).first()
        if empresa:
            raise forms.ValidationError(
                "La Empresa ya fue registrada previamente")
        return ruc

    def save(self, commit=True):
        """
        Metodo que guarda los datos de un usuario desde el formulario.

        :return: EmailUser creado
        """
        user = super(RegistroUsuarioAdministradorForm, self).save(commit=True)
        user.type_user = EmailUser.ADMIN
        user.empresa = self.get_empresa()
        user.set_password(self.cleaned_data["password1"])
        user.is_active = False
        if commit:
            user.save()
            datos_personales = DatosPersonales.objects.create(
                email_user=user,
                dni=self.cleaned_data['dni'],
                nombres=self.cleaned_data['nombres'],
                apellido_paterno=self.cleaned_data['apellido_paterno'],
                apellido_materno=self.cleaned_data['apellido_materno'],
                telefono=self.cleaned_data['telefono']
            )
            datos_personales.save()

        return user

    def get_empresa(self):
        apertura = datetime.time(hour=self.cleaned_data['hora_apertura'])
        cierre = datetime.time(hour=self.cleaned_data['hora_cierre'])
        empresa = Empresa.objects.create(
            ruc=self.cleaned_data['ruc'],
            nombre=self.cleaned_data['nombre_negocio'],
            direccion=self.cleaned_data['direccion'],
            telefono=self.cleaned_data['telefono_negocio'],
            tipo_servicio=self.cleaned_data['tipo_servicio'],
            representante=self.cleaned_data['representante'],
            hora_apertura=apertura,
            hora_cierre=cierre
        )
        return empresa


class RegistroUsuarioClienteForm(ModelForm):
    """
    Formulario que renderiza los campos de la pagina de registro de
    usuarios tipo Cliente.

    :cvar dni: Charfield que guarda el dni del usuario.
    :cvar nombres: Charfield que guarda los nombres del usuario.
    :cvar apellido_paterno: Charfield que guarda el apellido paterno del usuario.
    :cvar apellido_materno: Charfield que guarda el apellido materno del usuario.
    :cvar telefono: IntegerField que guarda el telefono del usuario.
    :cvar password1: Charfield que guarda el password del usuario.
    :cvar password2: Charfield que confirma el password del usuario.
    """
    dni = CharField(max_length=8,
                    widget=TextInput(attrs={'type': 'number'}),  label="DNI")
    nombres = CharField(max_length=256)
    apellido_paterno = CharField(max_length=64)
    apellido_materno = CharField(max_length=64)
    telefono = IntegerField()
    password1 = CharField(label='Password', widget=PasswordInput)
    password2 = CharField(
        label='Confirmar Contraseña', widget=PasswordInput)

    class Meta:
        model = EmailUser
        fields = (
            'email', 'password1', 'password2', 'dni', 'nombres',
            'apellido_paterno', 'apellido_materno', 'telefono',
        )

    def clean_password2(self):
        """
        Metodo que valida si las contraseñas coinciden.
        """
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("No Coinciden las Contraseñas")
        return password2

    def clean_dni(self):
        """
        Metodo que valida el campo de DNI el usuario.

        :return: dni.
        """
        dni = self.cleaned_data.get("dni")
        usuario = DatosPersonales.objects.filter(dni=dni).first()
        if usuario:
            raise forms.ValidationError("Existe un usuario con este dni.")
        return dni

    def save(self, commit=True):
        """
        Metodo que guarda los datos de un usuario desde el formulario.

        :return: EmailUser creado
        """
        user = super().save(commit=True)
        user.type_user = EmailUser.CLIENT
        user.set_password(self.cleaned_data["password1"])
        user.is_active = False
        if commit:
            user.save()
            datos_personales = DatosPersonales.objects.create(
                email_user=user,
                dni=self.cleaned_data['dni'],
                nombres=self.cleaned_data['nombres'],
                apellido_paterno=self.cleaned_data['apellido_paterno'],
                apellido_materno=self.cleaned_data['apellido_materno'],
                telefono=self.cleaned_data['telefono']
            )
            datos_personales.save()

        return user


class DatosPersonalesForm(ModelForm):
    """
    Formulario que renderiza los campos de la pagina de registro de datos
    personales.
    """

    class Meta:
        model = DatosPersonales
        fields = (
            'email_user',
            'dni',
            'nombres',
            'apellido_paterno',
            'apellido_materno',
            'telefono',
        )


class RegistroPreguntaUsuarioForm(ModelForm):
    """
    Formulario que renderiza los campos de la pagina de pregntas usuarios.

    :cvar email: CharField que guarda el email de un usuario a consultar.
    :cvar mensaje: Charfield que guarda el mensaje del usuario a consultar.
    :cvar tipo_servicio: Charfield que guarda el tipo servicio del usuario a
        consultar.
    """
    email = CharField(max_length=256, required=True)
    mensaje = CharField(
        max_length=256, required=True, widget=Textarea(
            attrs={'class': 'home-text-box', 'placeholder':
                'Quisiera saber sobre los protocolos de seguridad y sistema de '
                'puntos...',
               }))
    tipo_servicio = ModelChoiceField(
        queryset=TipoServicio.objects.all(), required=True)

    class Meta:
        model = PreguntaUsuario
        fields = ('email', 'mensaje', 'tipo_servicio',)

    def save(self, commit=True):
        """
        Metodo que guarda las preguntas de un usuario desde el formulario.

        :return: EmailUser creado
        """
        if commit:
            pregunta_usuario = PreguntaUsuario.objects.create(
                **self.cleaned_data)
            pregunta_usuario.save()

        return pregunta_usuario


class EditarContrasenaForm(ModelForm):
    """
    Formulario que renderiza los campos de la pagina de modificar contraseña.

    :cvar email: CharField que guarda el email de un usuario a consultar.
    :cvar password1: Charfield que guarda el password del usuario.
    :cvar password2: Charfield que confirma el password del usuario.
    """
    email = CharField(widget=TextInput(attrs={'readonly': 'readonly'}))
    password1 = CharField(label='Password', widget=PasswordInput)
    password2 = CharField(label='Confirmar Password', widget=PasswordInput)

    class Meta:
        model = EmailUser
        fields = (
            'email',
            'password1',
            'password2',
        )

    def clean_password2(self):
        """
        Metodo que valida si las contraseñas coinciden.
        """
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Las contraseñas no coinciden")
        return password2

    def save(self, commit=True):
        """
        Metodo que guarda los datos de un usuario desde el formulario.

        :return: EmailUser creado
        """
        user = super(EditarContrasenaForm, self).save(commit=True)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user
