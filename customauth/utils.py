from django.db.models import Q
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from customauth.tokens import account_activation_token
from servicios.models import Profesional, Empresa
from reservas.models import Reserva


def send_email_activar_cuenta(user):
    """
    Metodo usado para enviar un mensaje por correo a un usuario para que la
    cuenta sea activada.

    :param user: EmailUser que guarda el usuario al que se le enviara el correo.
    """
    mail_subject = 'Activa tu cuenta.'
    message = render_to_string('emails/emailuser_confirmation.html', {
        'user': user,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'token': account_activation_token.make_token(user),
    })
    to_email = user.email
    email = EmailMessage(mail_subject, message, to=[to_email])
    email.send()


class DisponibilidadProfesionales(object):
    """
    Clase para verificar la disponibilidad de reservas en un Profesional
    """
    def __init__(self, fecha, profesional, hora_i, hora_f):
        """
        :param fecha: datetime.date de la fecha para la reserva
        :param profesional: Profesional objeto que al que hay que sacarle las reservas
        :param hora_i: datetime.time hora inicio para hacer la reserva
        :param hora_f: datetime.time hora fin de la reserva
        """
        reservas = Reserva.objects.filter(profesional=profesional, fecha=fecha)
        self.reservas = reservas.filter(
            Q(inicio_hora_reserva__lt=hora_i, fin_hora_reserva__gt=hora_i) |
            Q(inicio_hora_reserva__lt=hora_f, fin_hora_reserva__gt=hora_f) |
            Q(inicio_hora_reserva__gte=hora_i, fin_hora_reserva__lte=hora_f)
        )

    def exists(self):
        return self.reservas.exists()


def get_profesionales_disponibles(
        empresa, fecha, hora_inicio, hora_fin, profesional_id=None):
    """
    Funcion que obtiene los profesionales disponibles a partir de un Profesional
    un horario y una empresa

    :param empresa: Empresa object a la que pertenecen las canchas
    :param fecha: datetime.date de la fecha para la reserva
    :param hora_inicio: datetime.time hora inicio para hacer la reserva
    :param hora_fin: datetime.time hora fin para la reserva
    :param profesional_id: Int id del Profesional que se esta evaluando
    :return: list con las canchas que estan disponibles
    """
    profesionales_disponibles = []
    otros_profesionales_disponibles = Profesional.objects.filter(
        empresa=empresa).exclude(id=profesional_id)
    # for que consulta canchas disponibles con el mismo horario
    for profesional in otros_profesionales_disponibles:
        reserva = DisponibilidadProfesionales(fecha, profesional, hora_inicio, hora_fin)
        if not reserva.exists():
            profesionales_disponibles.append(profesional)

    return profesionales_disponibles


def get_empresas_disponibles(
        fecha, hora_inicio, hora_fin=None,
        distrito=None, provincia=None, departamento=None):
    """
    Funcion que obtiene las empresas que tienen canchas disponibles en una
    fecha y en un horario

    :param fecha: datetime.date de la fecha que se quiere reservar
    :param hora_inicio: datetime.time del inicio de la reserva
    :param hora_fin: datetime.time hasta que hora se quiere reservar
    :param distrito: Str del nombre del distrito
    :param provincia: Str del nombre de la provincia
    :param departamento: Str del nombre del departamento
    :return: List de Empresa
    """
    kwargs_ubicacion = dict()
    if distrito:
        kwargs_ubicacion['distrito__nombre'] = distrito
    if provincia:
        kwargs_ubicacion['distrito__provincia__nombre'] = provincia
    if departamento:
        kwargs_ubicacion[
            'distrito__provincia__departamento__nombre'] = departamento
        #todo: hacer test de stress si es necesario
    empresas = Empresa.objects.filter(**kwargs_ubicacion)
    if not hora_fin:
        hora_fin = hora_inicio + 1
    empresas_disponibles = list()
    for empresa in empresas:
        for profesional in Profesional.objects.filter(empresa=empresa):
            reserva = DisponibilidadProfesionales(
                fecha, profesional, hora_inicio, hora_fin)
            if not reserva.exists():
                empresas_disponibles.append(empresa)
                break

    return empresas_disponibles

