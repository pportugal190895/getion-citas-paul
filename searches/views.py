from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import FormView, DetailView
from searches.forms import CompanySearchForm
from servicios.models import Empresa, Profesional


class CompanySearchFormView(FormView):
    """
    Vista del tipo FormView que implementa las pagina de busqueda de Empresas
    por nombre y por distrito .
    URL:
    http://localhost:8000/searches/searches/empresa/all/ (todas las Empresas)
    http://localhost:8000/searches/searches/empresa/"str_search"/ (con filtros)
    """
    form_class = CompanySearchForm
    template_name = 'search_company.html'

    def get_success_url(self, str_search):
        """
        Metodo encargado de redirecionar a la pagina con sus datos de entrada

        :param str_search: Cadena de teto que contendra los parametros de
            busqueda

        :return: Metodo reverse que redirecciona a la pagina actual con los
            resultados filtrados.
        """
        kwargs = {'str_search': str_search}
        return reverse('company-search', kwargs=kwargs)

    def get(self, request, str_search, *args, **kwargs):
        """
        Metodo que renderizara la pagina de busqueda y los resultados, a su vez
        interpretara la cadena de busqueda para pasarlos como argumentos a la
        consulta de la tabla Empresa.

        :param request: Parametro que guarda los datos de pagina.
        :param str_search: Parametro que guarda la cadena de texto con la
            busqueda
        
        :return: Metodo de renderizado del formulario y contexto de pagina.
        """
        if str_search == "all":
            # Filtro completo
            # Todo: filtrar por ubicacion
            contexto = {
                'empresas': Empresa.objects.all(), 'form': self.form_class}
            return render(self.request, self.template_name, context=contexto,)
        search_kwargs = {}
        for item in str_search.split(','):
            if item.split(':')[1] != '':
                search_kwargs[item.split(':')[0]] = item.split(':')[1]
        contexto = {
            'empresas': Empresa.objects.filter(**search_kwargs),
            'form': self.form_class
        }
        return render(self.request, self.template_name, context=contexto)

    def form_valid(self, form):
        """
        Metodo que recibe los campos de busqueda y redirecciona la  misma pagina
        con los resultados.

        :param form: Formulario ha validarse y de donde traeremos los criterios
            de busqueda.

        :return: Metodo de Redireccionamiento hacia la misma pagina.
        """
        str_search = ''
        if form.cleaned_data['distrito']:
            str_search += f"distrito__id:{form.cleaned_data['distrito'].id},"
        else:
            str_search = "distrito__id:,"
        str_search += f"nombre__icontains:{form.cleaned_data['nombre_empresa']}"

        if not form.cleaned_data['distrito'] and form.cleaned_data['nombre_empresa'] == '':
            # Si no se selecciona ningun campo filtrara todos las empresas
            return HttpResponseRedirect(self.get_success_url('all'))

        return HttpResponseRedirect(self.get_success_url(str_search))


class CompanyDetailView(DetailView):
    """
    Vista del tipo Detail View que rendrizara la pagina con los datos de una
    Empresa

    :cvar template_name: Variable que guarda el template html que sera
        renderizado.
    :cvar context_object_name: Variable que guarda el nombre el objeto del cual
        se quiere el detalle.
    :cvar model: Variable que guarda el modelo objeto que sera consultado.
    """
    template_name = 'detail_company.html'
    context_object_name = 'empresa'
    model = Empresa

    def get_context_data(self, **kwargs):
        contexto = super().get_context_data(**kwargs)
        empresa = self.get_object()
        contexto['profesionales'] = Profesional.objects.filter(empresa=empresa)
        return contexto
