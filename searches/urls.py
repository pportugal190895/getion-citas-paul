from django.urls import path
from searches.views import (
    CompanySearchFormView,
    CompanyDetailView,
)


urlpatterns = [
    path('searches/empresa/<str:str_search>/',
         CompanySearchFormView.as_view(), name='company-search'),
    path('searches/empresa/<int:pk>/detail/', CompanyDetailView.as_view(),
         name='company-detail')
]
