from django import forms
from servicios.models import Distrito, Provincia, Departamento


class CompanySearchForm(forms.Form):
    """
    Formulario que renderizara los campos de para la busqueda de una empresa

    :cvar departamento: Variable que guarda el campo del departamento.
    :cvar provincia: Variable que guarda el campo de la provincia.
    :cvar distrito: Variable que guarda el campo que buscara por el distrito
        de la empresa.
    :cvar nombre_empresa: Variable que guarda el campo que buscara por nombre
        de la empresa.
    """
    departamento = forms.ModelChoiceField(
        queryset=Departamento.objects.all(),
        required=False, empty_label='Departamento')
    provincia = forms.ModelChoiceField(
        queryset=Provincia.objects.all(),
        required=False, empty_label='Provincia')
    distrito = forms.ModelChoiceField(
        queryset=Distrito.objects.all(), required=False, empty_label='Distrito')
    nombre_empresa = forms.CharField(max_length=64, required=False)

    def __init__(self, *args, **kwargs):
        """
        Inicializador de la clase que cambia el estilo de los campos del
        formulario.
        """
        super(CompanySearchForm, self).__init__(*args, **kwargs)
        self.fields['departamento'].widget.attrs.update(
            {'class': 'form-control'})
        self.fields['provincia'].widget.attrs.update({'class': 'form-control'})
        self.fields['distrito'].widget.attrs.update({'class': 'form-control'})
        self.fields['nombre_empresa'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Nombre de Empresa'})
