from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView

from customauth.models import DatosPersonales

from enfermedad.forms import RegistroEnfermedadForm

class ContagioCreateView(CreateView):
    """
    Vista que implementara las estadisticas
    <!--            <a class="nav-link" href="{% url 'contagio_create' %}">
    <b>+</b></a>-->    """

    """
    Vista que hereda de FormView usado para renderizar la pagina de registro de
    contagio de un cliente.
    link: http://localhost:8000/enfermedad/contagio-reportar/

    :cvar form_class: Variable, guarda el formulario a ser usado.
    :cvar template_name: Variable, guarda el nombre del template a renderizar.
    """
    form_class = RegistroEnfermedadForm
    template_name = 'contagio_create.html'

    def get_success_url(self):
        """
        Metodo usado para redireccionar a la pagina de login cuando el
        formulario sea validado.

        :return: Metodo de redireccionamiento.
        """
        url_name = 'home-client-view'
        return reverse(url_name)

    def form_valid(self, form):
        """
        Metodo que valida el formulario, y actualizacion del campo email_user
        con que ingreso al sistema

        :param form: formulario a usarse.

        :return: Metodo de validacion del formulario.
        """
        form.instance.email_user = DatosPersonales.objects.get(
            email_user__id=self.request.user.id)
        enfermedad = form.save()
        print(form.instance.email_user)
        # return HttpResponseRedirect(self.get_success_url())
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        """
        :param kwargs:
        :return:
        """
        contexto=super(ContagioCreateView, self).get_context_data(**kwargs)
        contexto["usuario"]=DatosPersonales.objects.get(
            email_user__id=self.request.user.id)
        return contexto
