from bootstrap_datepicker_plus import DatePickerInput

from django.forms import (
    CharField,
    ModelForm,
    DateField,
)
from enfermedad.models import Cuarentena


class RegistroEnfermedadForm(ModelForm):
    """
    Formulario que renderiza los campos de la pagina de registro de
    usuarios tipo Cliente.

    Modelo que almacenara los detalles de ingresoa  cuanretena

    :cvar fecha_inicio: DateField que almacena la fecha de registro
    :cvar fecha_posible_contagio: DateField que almacena la fecha de
                                  posible contagio
    :cvar fecha_fin: DateField que almacena la fecha de fin de enfermedad
    :cvar email_user: ForeignKey que almacena el usuario que hizo la declaracion
    :cvar estado: CharField que almacena el estadi dek registro
    :cvar observaciones: CharField que almacena las observaciones
    """

    fecha_inicio = DateField(
        widget=DatePickerInput(format='%d/%m/%Y'))
    fecha_posible_contagio = DateField(
        widget=DatePickerInput(format='%d/%m/%Y'))
    observaciones = CharField(max_length=256)

    class Meta:
        model = Cuarentena
        fields = (
            'fecha_inicio', 'fecha_posible_contagio',
            'observaciones',
        )


