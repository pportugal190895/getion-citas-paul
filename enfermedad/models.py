from customauth.models import DatosPersonales
from django.db import models


class Cuarentena(models.Model):
    """
    Modelo que almacenara los detalles de ingresoa  cuanretena

    :cvar fecha_inicio: DateField que almacena la fecha de registro
    :cvar fecha_posible_contagio: DateField que almacena la fecha de
                                  posible contagio
    :cvar fecha_fin: DateField que almacena la fecha de fin de enfermedad
    :cvar email_user: ForeignKey que almacena el usuario que hizo la declaracion
    :cvar estado: CharField que almacena el estadi dek registro
    :cvar observaciones: CharField que almacena las observaciones
    """
    ACTIVADO = "1r"
    FINALIZADO = "2c"
    ANULADO = "3x"
    ESTADO_ENF = (
        (ACTIVADO, "Activado"),
        (FINALIZADO, "Finalizado"),
        (ANULADO, "Anulado"),
    )
    fecha_inicio = models.DateField()
    fecha_posible_contagio = models.DateField()
    fecha_fin = models.DateField(null=True)
    # profesional = models.ForeignKey(
    # Profesional, on_delete=models.CASCADE, null=True, related_name='reservas')
    email_user = models.ForeignKey(
        DatosPersonales, on_delete=models.CASCADE, null=True)
    estado = models.CharField(
        max_length=2, default=ACTIVADO, choices=ESTADO_ENF)
    observaciones = models.CharField(max_length=1024)

    class Meta:
        verbose_name_plural = 'Cuarentenas'
        verbose_name = 'Cuerentena'

    def __str__(self):
        return "{}, {}, {}, {}, {}".format(
            self.fecha_inicio, self.fecha_posible_contagio, self.fecha_fin,
            self.email_user, DatosPersonales.nombres)


