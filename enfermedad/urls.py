from django.urls import path

from enfermedad.views import ContagioCreateView


urlpatterns = [
    path('contagio-reportar/', ContagioCreateView.as_view(),
         name='contagio-create'),
    ]

